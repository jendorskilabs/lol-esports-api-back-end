package com.jendorskilabs.lol_e_sports.utilities

fun <T>determineDuplicates(arr:List<T>):Boolean = arr.size != arr.distinct().count()

fun <T>determineDuplicates(arr:Array<T>):Boolean = arr.size != arr.distinct().count()

fun <T>determineDuplicates(arr:Collection<T>):Boolean = arr.size != arr.distinct().count()
