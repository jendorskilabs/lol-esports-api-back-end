package com.jendorskilabs.lol_e_sports.model.matchModel.statsModel.perksModel

import org.springframework.data.annotation.TypeAlias

@TypeAlias("perks")
data class PerksModel(
        val gameID:String ?= null,
        val matchID: String ?= null,
        val rfc460Timestamp:String ?= null,
        val styleId: Long ?= null,
        val subStyleId: Long ?= null,
        val perkID: Long ?= null,
        val championID:String ?= null,
        val teamID:String ?= null,
        val teamColor:String ?= null,
        val summonerName:String ?= null,
        val participantID:String ?= null,
        val playerID: String ?= null
) {
    /**
    private String gameID;
    private String matchID;
    private String rfc460Timestamp;
    private long styleId;
    private long subStyleId;
    private long perkID;
    private String championID;
    private String teamID;
    private String teamColor;
    private String summonerName;//Paz
    private long participantID;
    private String playerID;
     */
}