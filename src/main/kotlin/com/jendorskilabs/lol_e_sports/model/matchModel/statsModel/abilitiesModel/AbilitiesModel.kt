package com.jendorskilabs.lol_e_sports.model.matchModel.statsModel.abilitiesModel

data class AbilitiesModel(
        val gameID:String,
        val rfc460Timestamp: String,
        val participantId: Long,
        val ability:String,
        val championID:String,
        val matchID:String,
        val summonerName: String,
        val summonerID: String,
        val teamID: String,
        val teamName: String
) {
    /**
    private String gameID;
    private String rfc460Timestamp;
    private long participantId;
    private String ability;
    private String championID;
    private String matchID;
     */

}