package com.jendorskilabs.lol_e_sports.model.matchModel.statsModel

import org.springframework.data.annotation.TypeAlias

/**I am making them "", so the DB returns only whats available.
 *
 * On the front-end, we will check for "". What is "", will be exempt.
 *
 * I believe the above assumption is wrong, i will confirm this later.
 *
 *
 * I need the gameID of every game here.
 * */
@TypeAlias("gamestats")
data class MatchStatsModel(
        val gameid : String,
        val datacompleteness : String,
        val url : String,
        val league : String,
        val year : String,
        val split : String,
        val playoffs : String,
        val date : String,
        val game : String,
        val patch : String,
        val playerid : String,
        val side : String,
        val position : String,
        val player : String,
        val team : String,
        val champion : String,
        val ban1 : String,
        val ban2 : String,
        val ban3 : String,
        val ban4 : String,
        val ban5 : String,
        val gamelength : String,
        val result : String,
        val kills : String,
        val deaths : String,
        val assists : String,
        val teamkills : String,
        val teamdeaths : String,
        val doublekills : String,
        val triplekills : String,
        val quadrakills : String,
        val pentakills : String,
        val firstblood : String,
        val firstbloodkill : String,
        val firstbloodassist : String,
        val firstbloodvictim : String,
        val teamKpm : String,
        val ckpm : String,
        val firstdragon : String,
        val dragons : String,
        val opp_dragons : String,
        val elementaldrakes : String,
        val opp_elementaldrakes : String,
        val infernals : String,
        val mountains : String,
        val clouds : String,
        val oceans : String,
        val dragonsUnknown: String,
val elders : String,
val opp_elders : String,
val firstherald : String,
val heralds : String,
val opp_heralds : String,
val firstbaron : String,
val barons : String,
val opp_barons : String,
val firsttower : String,
val towers : String,
val opp_towers : String,
val firstmidtower : String,
val firsttothreetowers : String,
val inhibitors : String,
val opp_inhibitors : String,
val damagetochampions : String,
val dpm : String,
val damageshare : String,
val wardsplaced : String,
val wpm : String,
val wardskilled : String,
val wcpm : String,
val controlwardsbought : String,
val visionscore : String,
val vspm : String,
val totalgold : String,
val earnedgold : String,
val earnedGpm : String,
val earnedgoldshare : String,
val goldspent : String,
val gspd : String,
val totalCs : String,
val minionkills : String,
val monsterkills : String,
val monsterkillsownjungle : String,
val monsterkillsenemyjungle : String,
val cspm : String,
val goldat10 : String,
val xpat10 : String,
val csat10 : String,
val opp_goldat10 : String,
val opp_xpat10 : String,
val opp_csat10 : String,
val golddiffat10 : String,
val xpdiffat10 : String,
val csdiffat10 : String,
val goldat15 : String,
val xpat15 : String,
val csat15 : String,
val opp_goldat15 : String,
val opp_xpat15 : String,
val opp_csat15 : String,
val golddiffat15 : String,
val xpdiffat15 : String,
val csdiffat15 : String
)

/*        val assists: String ?= "", // 4
        val ban1: String ?= "", // Qiyana
        val ban2: String ?= "", // Gangplank
        val ban3: String ?= "", // Akali
        val ban4: String ?= "", // Elise
        val ban5: String ?= "", // Olaf
        val championName: String ?= "", // Gnar
        val combinedKillsPerMinute: String ?= "", // 1.092964824
        val creepScoreAtFifteenMinutes: String ?= "", // 116
        val creepScoreAtTenMinutes: String ?= "", // 76
        val creepScoreDiffAtTenMinutes: String ?= "", // -14
        val creepScoreDifferenceAtFifteenMinutes: String ?= "", // -6
        val creepScorePerMinute: String ?= "", // 7.085427136
        val date: String ?= "", // 10/2/19 6:52 AM, we have to find a way around this, such that we can parse also for time like 2020-02-16T11:57:00.000Z
        val deaths: String ?= "", // 4
        val doubleKills: String ?= "", // 0
        val experienceDifferenceAtTenMinutes: String ?= "", // -521
        val experienceEarnedAtTenMinutes: String ?= "", // 4530
        val firstBaronKilled: String ?= "", // 0
        val firstBaronTimeKilled: String ?= "", // 23.77981667
        val firstBloodAssist: String ?= "", // 0
        val firstBloodKills: String ?= "", // 0
        val firstBloodTime: String ?= "", // 3.879983333
        val firstBloodVictim: String ?= "", // 0
        val firstDragonTimeKill: String ?= "", // 9.717566667
        val firstTeaMidLaneKilled: String ?= "", // 0
        val firstTeamThreeTowerKilled: String ?= "", // 0
        val firstTowerKilled: String ?= "", // 0
        val firstTowerTimeKilled: String ?= "", // 13.58856667
        val firtDragonKill: String ?= "", // 0
        val gameCount: String ?= "", // 1
        val gameID: String ?= "", // 1070340
        val gameLength: String ?= "", // 26.53333333
        val goldDifferenceAtFifteenMinutes: String ?= "", // -798
        val goldDifferenceAtTenMinutes: String ?= "", // -362
        val goldEarnedPerMinute: String ?= "", // 184.059799
        val goldSpentPercentDifference: String ?= "", // -0.151447661
        val heraldKillTime: String ?= "",
        val kills: String ?= "", // 0
        val killsPerMinute: String ?= "", // 0
        val laneMinionsKilled: String ?= "", // 180
        val league: String ?= "", // WC
        val neutralMonsterKilledInOwnTeam: String ?= "", // 4
        val neutralMonstersKilled: String ?= "", // 8
        val neutralMonstersKilledInOpposingTeam: String ?= "", // 1
        val opponentKillsPerMinute: String ?= "", // 0.037688442
        val opposingCreepScoreAtFifteenMinutes: String ?= "", // 122
        val opposingCreepScoreAtTenMinutes: String ?= "", // 90
        val opposingTeamExperiencEarnedAtTenMinutes: String ?= "", // 5051
        val opposingTeamInvisibleWardsClearRate: String ?= "",
        val opposingTeamVisibleWardsClearRate: String ?= "",
        val opposingTotalGoldEarnedAtFifteenMinutes: String ?= "", // 5468
        val opposingTotalGoldEarnedAtTenMinutes: String ?= "", // 3440
        val patchNumber: String ?= "", // 9.19
        val pentaKills: String ?= "", // 0
        val player: String ?= "", // Evi, or also known as playerName
        val playerID: String ?= "", // 1, this should have been participantID
        val position: String ?= "", // Top
        val quadraKills: String ?= "", // 0
        val result: String ?= "", // 0
        val riftHeraldTaken: String ?= "", // 0
        val shareOfTeamTotalDealtToChampions: String ?= "", // 0.312067433
        val shareOfTeamTotalGold: String ?= "", // 0.206844383
        val shareOfTeamTotalWards: String ?= "", // 0.105769231
        val side: String ?= "", // Blue
        val teamName: String ?= "", // DetonatioN FocusMe
        val teamdeaths: String ?= "", // 22
        val teamkills: String ?= "", // 7
        val timePeriod: String ?= "", // 2019-W
        val totalBaronKilledByOpposingTeam: String ?= "", // 1
        val totalBaronKilledByTeam: String ?= "", // 0
        val totalDamageDealtToChampions: String ?= "", // 11588
        val totalDamageDealtToChampionsPerMinute: String ?= "", // 436.7336683
        val totalDragonsKilledByOpposition: String ?= "", // 2
        val totalDragonsKilledByTeam: String ?= "", // 1
        val totalElderDragonsKilledByOpposingTeam: String ?= "", // 0
        val totalElderDragonsKilledByTeam: String ?= "", // 0
        val totalElementalDrakesKilledByOpposingTeam: String ?= "", // 0
        val totalElementalDrakesKilledByTeam: String ?= "", // 0
        val totalGoldEarned: String ?= "", // 8407
        val totalGoldEarnedAtFifteenMinutes: String ?= "", // 4670
        val totalGoldEarnedAtTenMinutes: String ?= "", // 3078
        val totalMountainDrakesKilledByTeam: String ?= "", // 0
        val totalTowersKilledByOpposingTeams: String ?= "", // 10
        val totalTowrsKilledByTeam: String ?= "", // 1
        val totalWardsKilledCleared: String ?= "", // 4
        val totalWardsKilledClearedPerMinute: String ?= "", // 0.150753769
        val totalWardsPlaced: String ?= "", // 11
        val totalWardsPlacedPerMinute: String ?= "", // 0.414572864
        val totalWaterDrakesKilledByTeam: String ?= "", // 0
        val tripleKills: String ?= "", // 0
        val url: String ?= "", // https://matchhistory.euw.leagueoflegends.com/en/#match-details/ESPORTSTMNT06/1070340?gameHash=3e0204c000597715&tab=overview
        val visionControlWardsPlaced: String ?= "", // 4
        val visionControlWardsPurchased: String ?= "", // 6
        val week: String ?= "", // PI-RR
        /**Stats From Vincent's API, the likes of kills, deaths, assists are already available*/
        val creepScore: String ?= "",
        val level: Long ?= 0,
        val currentHealth: Long ?= 0,
        val maxHealth: Long ?= 0,
        val killParticipation: Double ?= 0.0,
        val championDamageShare: Double ?= 0.0,
        val wardsPlaced: Long ?= 0,
        val wardsDestroyed: Long ?= 0,
        val attackDamage: Long ?= 0,
        val abilityPower: Long ?= 0,
        val criticalChance: Long ?= 0,
        val attackSpeed: Long ?= 0,
        val lifeSteal: Long ?= 0,
        val armor: Long ?= 0,
        val magicResistance: Long ?= 0,
        val tenacity: Long ?= 0,
        val teamTotalGold: Long ?= 0,
        val teamInhibitors: Long ?= 0,
        val teamTowers: Long ?= 0,
        val teamBarons: Long ?= 0,
        val teamID: String ?= "",
        val championID: String ?= "",
        val summonerID: String ?= "" /**The actual player ID, i mean the summoner Identification number from vincent, also known to be the playerID, different from the participantID in the game.*/

*/