package com.jendorskilabs.lol_e_sports.model.matchModel.statsModel.itemsModel

import org.springframework.data.annotation.Id
import org.springframework.data.annotation.TypeAlias

/**Items are a player stats, so the */
@TypeAlias("items")
data class ItemsModel(
        val gameID:String ?= null,
        val matchID:String ?= null,
        @Id val rfc460Timestamp:String ?= null,
        val participantID:String ?= null,
        val itemID:String ?= null,
        val championID:String ?= null,
        val teamID:String ?= null,
        val teamColor:String ?= null,
        val summonerName:String ?= null,
        val playerID:String ?= null
) {
    /**
    private String gameID;
    private String matchID;
    private String rfc460Timestamp;
    private long participantID;
    private long itemID;
    private String championID;
    private String teamID;
    private String teamColor;
    private String summonerName;//Paz
    private String playerID;//Paz
     */
}