package com.jendorskilabs.lol_e_sports.model.matchModel.statsModel.dragonsModel

import org.springframework.data.annotation.TypeAlias

/**Dragons are a team stats, not a player stats,
 * so the championID, participantID and the summonerName won't be available
 * */
@TypeAlias("dragons")
data class DragonsModel(
        val dragonName:String,
        val gameID:String,
        val matchID: String,
        val rfc460Timestamp:String,
        val teamColor:String,
        val teamID:String
/*
        ,val championID: String,
        val participantID:String,
        val summonerName:String
*/
){
}