package com.jendorskilabs.lol_e_sports.model.matchModel

import org.springframework.data.annotation.Id
import org.springframework.data.annotation.TypeAlias
import org.springframework.data.mongodb.core.mapping.Document
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull

/**
 * This model will be used for the standings,
 *
 * This model imitates MatchStandingsModel, CompletedEventMatchModel.
 *
 * I may use this model for the Schedule Matches, from ScheduleMatchModels, MatchEventDetailsModel,
 *
 *
 * I made them all null, except for the annotated @Id, so thats the only variable that cannot be blank,
 * so when pulling them out, the DB automatically wont show the nulls in the response,
 * that way, we have a lighter load on the server.
 *
 * So, new observation: Match should have blueTeam and redTeam info, or remove the @Id there.
 * So Mongo will provide the ID at its end.
 *
 * @Id not needed.
 * I have changed it back to @Id needed.
 *
 * I am assigning them [var] so i can be able to change them later on.
 * */
@TypeAlias("match")
@Document("match")
data class Match(
        //@Id @NotBlank(message = "gameID cannot be blank") val gameID: String,
        //@Id var id: Int == 0 ,
        @Id
        @NotBlank(message = "matchID cannot be blank")
        @NotNull
        var matchID: String,
        var gameID: String ?= "",
        var matchType:String ?= "",
        var eventStartTime:String ?= "",
        var eventBlockName:String ?= "",
        var eventLeagueName:String ?= "",
        var matchStrategyType:String ?= "",
        var matchStrategyCount:Long ?= 0,
        var tournamentID: String ?= "",
        var leagueID:String ?= "",
        var eventLeagueSlug:String ?= "",
        var leagueName: String ?= "",
        var leagueImageURL:String ?= "",
        var hasVods:Boolean ?= false,
        var sectionName:String ?= "",
        var stageName:String ?= "",
        var stageSlug:String ?= "",
        var stageType:String ?="",
        var previousMatchIdString:String ?= "",
        var matchState:String ?= "",//Could be completed, unneeded
        var type: String ?= "",
        var gameWins: Long ?= 0,
        var matchCount: Long ?= 0,
        var videoURL: String = ""
        )