package com.jendorskilabs.lol_e_sports.model.recordsModel.teamRecordsModel

import org.springframework.data.annotation.Id
import org.springframework.data.annotation.TypeAlias
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotEmpty

/**
 * What this means is the record of the team for this tournament, its standings especially,
 * i.e. its position amongst the teams in the tournament.
 *
 * I want to use this same model for the teams in the Standings, to add the teamResults,
 *
 * I also want to use this same model for the teams in Schedule, to add the result and the records
 *
 * This model imitates RecordStandingsModel, ColorTeamModel
 * */
@TypeAlias("teamRecords")
data class TeamRecords(
        @Id val teamID: String ?= null,
        val teamSlug: String ?= null,
        val wins: Long ?= null,
        val loss: Long ?= null,
        val sectionName: String ?= null,
        val stageName: String ?= null,
        val stageSlug:String ?= null,
        val stageType:String ?= null,
        val tournamentID: String ?= null,
        val matchID: String ?= null,
        val gameWins: Long ?= null,
        //@NotBlank(message = "The ordinal is the position of the team in the standings, it cannot be zero or empty")
        //@NotEmpty(message = "The ordinal is the position of the team in the standings, it cannot be zero or empty")
        val ordinal: Long, //,val matchID: String
        val totalGold: Long ?= null,
        val inhibitors: Long ?= null,
        val towers: Long ?= null,
        val barons: Long ?= null,
        val totalKills: Long ?= null,
        val eventID: String ?= null,
        val teamColor: String ?= null,
        val rfc460Timestamp: String ?= null,
        val gameState: String ?= null,
        val patchVersion: String ?= null

){

    /**
     *
     */
}