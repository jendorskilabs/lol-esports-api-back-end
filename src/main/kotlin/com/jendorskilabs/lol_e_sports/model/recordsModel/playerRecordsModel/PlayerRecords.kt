package com.jendorskilabs.lol_e_sports.model.recordsModel.playerRecordsModel

import com.sun.javafx.beans.IDProperty
import org.springframework.data.annotation.Id

/**
 * I will be using this one for the player stats from this endpoint:
 * https://feed.lolesports.com/livestats/v1/window/103540364360693574?startingTime=2020-02-16T11:57:00.000Z
 *
 * */
data class PlayerRecords(
        @Id val playerID: String ?= null
        //val participantID: String ?= null
){

}