package com.jendorskilabs.lol_e_sports.model.videoModel

import org.springframework.data.annotation.Id
import org.springframework.data.annotation.TypeAlias
import javax.validation.constraints.NotEmpty

/**
 * The Video Model should most importantly contain the parameter; the link that we will use to load the video itself.
 * the videos are usually on youtube.
 *
 * The latest issue is whether
 *
 * This video imitates CompletedEventVodModel, VodModel
 *
 * The constructor is preferable in the event that more fields are
 * needed.
 *
 * @Id needed for Videos
 * */
@TypeAlias("video-replay")
data class Video(
        val videoID: String ?= "",
        @Id val parameter: String,
        val locale: String ?= "",
        val provider: String ?= "",
        val offset: Int ?= 0,
        @NotEmpty(message = "matchID cannot be empty for videos") val matchID: String ?= "",//get video by matchID
        val gameID: String ?= "",//getVideo by gameID
        val gameState: String ?= "",
        val gameNumber: String ?= "",
        val tournamentID:String ?= "",//get video by tournamentID
        val leagueID:String ?= "",//get video by leagueID
        val leagueSlug:String ?= "",//getVideo by leagueSlug
        val leagueName: String ?= "",//get video by leagueName
        val leagueImageURL: String ?= "",//
        val matchCount:String ?= "",
        val eventStartTime: String ?= "",
        val eventLeagueName: String ?= "",//get Video by leagueName or eventLeagueName
        val eventBlockName: String ?= "",
        val matchType: String ?= "",
        val matchStrategyType: String ?= "",
        val matchStrategyCount: String ?= ""
) {

    /**
     * This constructor is for the Video Model with detailed information about it
     *"id": "104140140132888853",
    "parameter": "JLTkm1EodyY",
    "locale": "pt-BR",
    "provider": "youtube",
    "offset": 0
     */

    constructor(parameter: String,
    id: String,
    locale: String,
    provider: String,
    offset: String):this(parameter = "")

    /**
     * This constructor is for videos
     * whose events(matches) have been completed
     * */
    constructor(parameter: String,
    gameID: String,
    matchID: String,
    eventBlockName: String,
    eventStartTime: String,
    eventLeagueName:String,
    videoParameter:String,
    matchType:String,
    matchStrategyCount:Long,
    matchStrategyType:String):this(parameter = "")

/*
    fun toDto():VideoDto = VideoDto()

    companion object{
        fun fromDto(dto: VideoDto){

        }
    }
*/

}