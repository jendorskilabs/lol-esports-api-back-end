package com.jendorskilabs.lol_e_sports.model.playerModel

import org.springframework.data.annotation.TypeAlias
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotEmpty

/**
 * This is the model for a player in E-Sports
 * game stats for the player should NOT be here.
 *
 * Stats like:
 * kills, deaths, assists, gold earned, gold spent,
 *
 * tournament kills,
 * tournament kills,
 * tournament(s) the player is currently playing in,
 * team(s) the player is in,
 *
 * This model imitates LeaguePlayerModel
 * @Id not needed pls
 *
 * So i discovered that if the full fields are not there, it will
 * throw a PropertyReferenceException.
 * So also adding the equals (=) with an empty string ensures that even if the fields are not in the request body,
 * its represented with an empty string
 *
 * So we can have several instances of this player with several differing stats.
 * So all the tournament stats, all time time stats, gamestats, headToHeadStats, will all be here?, NO, it wont be here.
 *
 * I believe they will all be in the @link PlayerRecords.
 * The same thing for teams
 *
 * */

@TypeAlias("player")
data class Player @JvmOverloads constructor(
        @NotEmpty(message = "playerID for the player must not be empty")
        var playerID: String,

        @NotEmpty(message = "The summonerName of the player must not be empty")
        @NotBlank(message = "SummonerName must not be blank")
        var summonerName: String,

        var firstName: String = "",

        var lastName: String= "",

        var image: String= "",

        var role: String= "",

        var homeLeagueName: String = "",

        var teamID: String = "",

        var teamSlug: String = "",

        var teamCode: String= "",

        var teamName: String? = "",

        var leagueID: String = ""
) {

    /**Constructor, for a tournament information and all stats for that tournament for the player
     *
     * Contains the team info the player belongs to in that tournament
     *
     * We should also be able to add the player Best stats, no?
     * */
    constructor(playerID: String,//100205572901889771
                summonerName: String,//Paz
                firstName: String,//Shiro
                lastName: String,//Sasaki
                image: String,//https://lolstatic-a.akamaihd.net/esports-assets/production/player/paz-gbmgjqdi.png
                role: String,//top
                tournamentID: String,
                tournamentTotalKills: Int,
                tournamentTotalDeaths: Int,
                tournamentTotalAssists: Int,
                teamID: String,
                teamSlug: String,
    leagueID: String): this(playerID, summonerName, firstName, lastName, image, role)

    constructor(
            playerID: String,//100205572901889771
            summonerName: String,//Paz
            firstName: String,//Shiro
            lastName: String,//Sasaki
            image: String,//https://lolstatic-a.akamaihd.net/esports-assets/production/player/paz-gbmgjqdi.png
            role: String,//top
            teamName: String,
                teamID: String ,
                teamSlug: String ,
                homeLeagueName: String ,
                homeLeagueRegion: String ,
                teamCode: String ,
                leagueID: String):this(playerID, summonerName, firstName, lastName, image, role)

    /**
     * And now, a constructor, to hold the stats of a player in a match
     * It should hold the participantId, the summonerName, and the match stats of the player
     *
     * This constructor is for the
     * */
    constructor(playerID:String,
                summonerName: String,
                firstName: String,
                lastName: String,
                image: String,
                role: String,
                leagueID: String,
                gameID:String,
                matchID: String,
                patchVersion: String,
                teamColor: String,
                participantID: Long,
                championID: String,
                rfc460Timestamp: String,
                level: Long,
                kills: Long,
                deaths:Long,
                assists: Long,
                creepScore: Long,
                totalGold: Long,
                currentHealth: Long,
                maxHealth:Long,
                totalGoldEarned: Long,
                killParticipation: Long,
                championDamageShare: Long,
                wardsPlaced: Long,
                wardsDestroyed: Long,
                attackDamage: Long,
                abilityPower: Long,
                criticalChance: Long,
                attackSpeed: Long,
                lifeSteal: Long,
                armour:Long,
                magicResistance: Long,
                tenacity: Long,
                //items:
                //perkMetaData
                //abilities
                teamTotalGold: Long,
                teamInhibitors: Long,
                teamTowers: Long,
                teamBarons: Long,
                teamTotalKills: Long):
            this(playerID, summonerName, firstName, lastName, image, role)

    /**
     * Constructor to hold stats for the match events
     * */

    /**
     *
    @SerializedName("items")
    @Expose
    private List<String> items;

    @SerializedName("perkMetadata")
    @Expose
    @ForeignKey(saveForeignKeyModel = true, stubbedRelationship = true)
    private LiveStatsPerkMetadataModel perkMetadata;

    @SerializedName("abilities")
    @Expose
    private List abilities;
     */


/*
    constructor(age: Int): this("Anon", age) {
    }
*/

/*data class KeyTag(val a: String, val b: Int, val c: Double) {
    companion object Factory {
        val empty = KeyTag("", 0, 0.0)

        fun create(bigString: String): KeyTag {
            // Logic to extract appropriate values for arguments a, b, c
            return KeyTag(a, b, c)
        }

        fun bake(i: Int): KeyTag = KeyTag("$i", i, i.toDouble())
    }
}
Usage is then:

val ks = KeyTag.create("abc:1:10.0")
val ke = KeyTag.empty
val kb = KeyTag.bake(2)
*/

}