package com.jendorskilabs.lol_e_sports.model.baseModel

/**
 * No need to add the HttpHeaders, to the responseBody, it should be added instead to
 * the responseHeaders
 * */
data class BaseResponseModel<out T> (
        val httpStatus: Int,
        val responseMessage: String = "Successful Response",
        //val responseHeader: HttpHeaders = HttpHeaders(),
        val response: T) {

    /**FOR PAGINATED INFORMATION
     *
     * I am tired, my eyes are hurt
     * */
    constructor(httpStatus: Int, responseMessage: String, response: T, page: Int, size: Int):this(httpStatus, responseMessage, response)

}

/**
 * This is used in the Controllers
 * */
data class PaginatedBaseResponseModel<out T>(val httpStatus: Int, val responseMessage: String = "Successful Response", val response: T, val page: Int, val size: Int)

/**
 * This is used in the Services Classes
 * */
data class PaginatedResponseModel<out T>(val response: T, val page:Int, val size:Int)