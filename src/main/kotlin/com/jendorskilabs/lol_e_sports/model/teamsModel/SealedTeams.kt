package com.jendorskilabs.lol_e_sports.model.teamsModel

sealed class SealedTeams(
        open val teamSlug:String
) {

    class MatchTeams(override val teamSlug: String):SealedTeams(teamSlug)

}