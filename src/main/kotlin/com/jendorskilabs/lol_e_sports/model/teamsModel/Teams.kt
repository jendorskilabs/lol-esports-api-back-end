package com.jendorskilabs.lol_e_sports.model.teamsModel

import org.springframework.data.annotation.TypeAlias
import javax.validation.constraints.NotEmpty

/**
 * adding {@link Indexed} doesnt help, the best thing would be to include all props and initialise them as either empty strings
 * or false if Boolean, null if Int or Long etc, rather than using null.
 *
 * Having several constructors wont help at all.
 *
 * And the variable names, should be the same as the one from where i am getting it from.
 *
 * Oh Dear.
 *
 * If you add the @Id to any of the props, the name changes to _id. i dont want this.
 *
 * If any of the props is null, it wont get saved to the MongoDB.
 *
 * This model will take everything from all the models it imitates.
 *
 * This model imitates TeamStandingsModel and Team_StandingsModel, CompletedEventTeamModel, ScheduleTeamsModel,
 *                      TeamEventDetailsModel, Team_EventDetailsModel
 *
 * @Id not needed for Teams. Let MongoDB put its own ID there.
 * */
@TypeAlias("teams")
class Teams(
        val teamImage: String,
        @NotEmpty(message = "The name of the team must be available") val teamName: String,//Mmo
        val teamID: String,//123, 456
        val teamSlug: String ?= null,
        val teamCode: String ?= null,//or teamSlug
        val alternativeImage: String ?= null,
        val homeLeagueName: String ?= null,
        val homeLeagueRegion: String ?= null,
        val backgroundImage:String ?= null,
        val tournamentID: String ?= null,
        val sectionName: String ?= null,
        val stageName:String ?= null,
        val stageSlug: String ?= null,
        val stageType: String ?= null,
        val ordinal: Long ?= null,
        val matchID: String ?= null,
        val name: String ?= null,
        val eventStartTime:String ?= null,
        val eventBlockName:String ?= null,
        val eventLeagueName:String ?= null,
        val matchStrategyType:String ?= null,
        val matchStrategyCount:Long ?= null,
        val teamGameWins: Long ?= null,
        val hasVods: Boolean ?= null,
        val leagueSlug: String ?= null,
        val teamOutCome: String ?= null,
        val teamWins: Long ?= null,
        val teamLosses: Long ?= null,
        val side:String ?= null,
        val leagueImageURL: String ?= null
        ) {

        /**
         * Constructor for team result when we check the match they play.
         * This constructor is for the League Schedule
         * */
        constructor(teamImage: String,
                    teamName: String,
                    teamID: String,
                    teamSlug: String,
                    teamCode: String,
                    alternativeImage: String,
                    homeLeagueName: String,
                    homeLeagueRegion: String,
                    homeLeagueSlug:String,
                    homeLeagueID: String,//Id of the league
                    teamOutCome: String,//win or loss
                    teamGameWin: Int,//1 or null
                    teamWins: Int,//The number of wins of the team
                    teamLoss: Int,//The number of losses of the team
                    matchID: String,//The ID of the match
                    hasVods:Boolean,//If there is a video of the match
                    matchType: String,//the type of the match, e.g. bestOf
                    matchCount: Int,
                    matchStartTime: String,
                    eventBlockName: String
        ):this(teamImage, teamName, teamID, teamSlug, teamCode, alternativeImage, homeLeagueName, homeLeagueRegion)

    /**
         * This constructor is for the team standings information,
         * which explains the outcome of the team in a match, and the number of wins for that match
         *
         * */
        constructor(image: String,
                    name: String,
                    id: String,
                    slug: String,
                    code: String,
                    alternativeImage: String,
                    homeLeagueName: String,
                    homeLeagueRegion: String,
                    matchID:String,
                    tournamentID: String,
                    sectionName: String,
                    stageName: String,
                    stageSlug: String,
                    stageType: String,
                    outcome:String,
                    gameWins: Long):this(image, name, id, slug, code, alternativeImage, homeLeagueName, homeLeagueRegion)

    /**
         * This is for teams record information,
         * showing the teams position with respect to the tournament and
         * stage information.
         */
        constructor(image: String,
                    name: String,
                    id: String,
                    slug: String,
                    code: String,
                    alternativeImage: String,
                    homeLeagueName: String,
                    homeLeagueRegion: String,
                    tournamentID: String,
                    stageName: String,
                    sectionName: String,
                    stageSlug: String,
                    stageType: String,
                    ordinal: Long ):this(image, name, id, slug, code, alternativeImage, homeLeagueName, homeLeagueRegion)

    /**
         * This constructor is for teams that have completed an event (or match)
         * The gameWins, eventStartTime, matchID, strategy and the matchType are all available here
         * */
        constructor(image: String,
                    name: String,
                    id: String,
                    slug: String,
                    code: String,
                    gameWins: Long,
                    matchID: String,
                    matchType: String,
                    eventStartTime: String,
                    eventLeagueName:String,
                    eventBlockName:String,
                    teamStrategyCount:Long,
                    teamStrategyType: String):this(image, name, id, slug, code, "null", "null", "null")

        /**
         * This constructor is for the TeamsList in the event details
         * */
        constructor(
                teamImage: String,
                @NotEmpty(message = "The name of the team must be available")
                teamName: String,
                teamID: String,
                teamSlug: String,
                teamCode: String,
                alternativeImage: String,
                homeLeagueName: String,
                homeLeagueRegion: String,
                matchCount:String,
                teamGameWins:Long,
                matchID:String,
                tournamentID:String,
                leagueID:String,
                leagueSlug:String
        ):this(teamImage, teamName, teamID, teamSlug, teamCode, alternativeImage, homeLeagueName, homeLeagueRegion)

}