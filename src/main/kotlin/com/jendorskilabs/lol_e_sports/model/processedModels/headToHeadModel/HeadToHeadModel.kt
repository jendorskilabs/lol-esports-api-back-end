package com.jendorskilabs.lol_e_sports.model.processedModels.headToHeadModel

import org.springframework.data.annotation.Id

/**
 * This model will be used for finding the comparisons between teams, players
 *
 * And maybe tournaments
 * */
data class HeadToHeadModel(
        @Id val id: String
)