package com.jendorskilabs.lol_e_sports.model.errorModel

import com.fasterxml.jackson.annotation.JsonIgnore
import java.time.Instant
import java.time.format.DateTimeFormatter
import java.util.*

/**
 * This is the default app error,
 * so if any nonsense request is made,
 * we can put up an error message using this model.
 * */
class DefaultAppError(val apiVersion: String, val code: Int, message: String, val domain: String,
                      val reason: String, val errorMessage: String, errorReportUri: String, timestamp: String) {

    private val error: ErrorBlock
    fun toAttributeMap(): Map<String, Any> {
        return mapOf(
                "httpStatus" to code,
                "apiVersion" to apiVersion,
                "domain" to domain,
                "reason" to reason,
                "responseMessage" to errorMessage,
                "timestamp" to DateTimeFormatter.ISO_INSTANT.format(Instant.now())//instead of errorMessage, make it responseMessage, so its consistent
                //"error" to error
        )
    }

    private class ErrorBlock {
        @JsonIgnore
        val uniqueId: UUID
        val code: Int
        val message: String
        val errors: List<Error>

        constructor(code: Int, message: String, domain: String?,
                    reason: String?, errorMessage: String?, errorReportUri: String) {
            this.code = code
            this.message = message
            uniqueId = UUID.randomUUID()
            errors = listOf(
                    Error(domain, reason, errorMessage, "$errorReportUri?id=$uniqueId")
            )
        }

        private constructor(uniqueId: UUID, code: Int, message: String, errors: List<Error>) {
            this.uniqueId = uniqueId
            this.code = code
            this.message = message
            this.errors = errors
        }

        companion object {
            fun copyWithMessage(s: ErrorBlock, message: String): ErrorBlock {
                return ErrorBlock(s.uniqueId, s.code, message, s.errors)
            }
        }
    }

    private class Error(val domain: String?, val reason: String?, val message: String?, val sendReport: String)

    companion object {
        fun fromDefaultAttributeMap(apiVersion: String,
                                    defaultErrorAttributes: Map<String?, Any>,
                                    timestamp: String = DateTimeFormatter.ISO_INSTANT.format(Instant.now()),
                                    sendReportBaseUri: String): DefaultAppError {
            // original attribute values are documented in org.springframework.boot.web.servlet.error.DefaultErrorAttributes
            return DefaultAppError(
                    apiVersion,
                    (defaultErrorAttributes["status"] as Int),
                    (defaultErrorAttributes["message"] ?: "no message available") as String,
                    (defaultErrorAttributes["path"] ?: "no domain available") as String,
                    (defaultErrorAttributes["error"] ?: "no reason available") as String,
                    defaultErrorAttributes["message"] as String,
                    timestamp,
                    sendReportBaseUri
            )
        }
    }

    init {
        error = ErrorBlock(code, message, domain, reason, errorMessage, errorReportUri)
    }
}