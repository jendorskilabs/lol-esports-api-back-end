package com.jendorskilabs.lol_e_sports.model.errorModel

import com.fasterxml.jackson.annotation.JsonIgnore
import java.util.*

class ErrorModel(val apiVersion: String, val code: String, message: String, domain: String?,
                 reason: String?, errorMessage: String?, errorReportUri: String) {

    private val error: ErrorBlock

    // utility method to return a map of serialized root attributes,
    // see the last part of the guide for more details
    fun toAttributeMap(): Map<String, Any> {
        return mapOf(
                "apiVersion" to apiVersion,
                "error" to error
        )
    }

    private class ErrorBlock {

        @JsonIgnore
        val uniqueId: UUID

        val code: String

        val message: String

        val errors: List<Error>

        constructor(code: String, message: String, domain: String?,
                    reason: String?, errorMessage: String?, errorReportUri: String) {
            this.code = code
            this.message = message
            uniqueId = UUID.randomUUID()
            errors = listOf(
                    Error(domain, reason, errorMessage, "$errorReportUri?id=$uniqueId")
            )
        }

        private constructor(uniqueId: UUID, code: String, message: String, errors: List<Error>) {
            this.uniqueId = uniqueId
            this.code = code
            this.message = message
            this.errors = errors
        }

        companion object {
            fun copyWithMessage(s: ErrorBlock, message: String): ErrorBlock {
                return ErrorBlock(s.uniqueId, s.code, message, s.errors)
            }
        }
    }

    private class Error(val domain: String?, val reason: String?, val message: String?, val sendReport: String)

    companion object {
        fun fromDefaultAttributeMap(apiVersion: String,
                                    defaultErrorAttributes: Map<String?, Any>,
                                    sendReportBaseUri: String): ErrorModel {
            // original attribute values are documented in org.springframework.boot.web.servlet.error.DefaultErrorAttributes
            return ErrorModel(
                    apiVersion,
                    (defaultErrorAttributes["status"] as Int?).toString(),
                    (defaultErrorAttributes["message"] ?: "no message available") as String,
                    (defaultErrorAttributes["path"] ?: "no domain available") as String,
                    (defaultErrorAttributes["error"] ?: "no reason available") as String,
                    defaultErrorAttributes["message"] as String?, sendReportBaseUri
            )
        }
    }

    init {
        error = ErrorBlock(code, message, domain, reason, errorMessage, errorReportUri)
    }
}