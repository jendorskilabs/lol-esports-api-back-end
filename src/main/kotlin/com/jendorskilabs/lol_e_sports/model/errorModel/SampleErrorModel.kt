package com.jendorskilabs.lol_e_sports.model.errorModel

import java.time.Instant
import java.time.format.DateTimeFormatter

data class SampleErrorModel(
        val httpStatus: Int,
        val responseMessage: String?,//instead of errorMessage, we use responseMessage
        val apiVersion: String,
        val timestamp: String = DateTimeFormatter.ISO_INSTANT.format(Instant.now())
)