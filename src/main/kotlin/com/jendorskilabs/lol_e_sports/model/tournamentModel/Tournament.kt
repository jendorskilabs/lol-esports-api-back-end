package com.jendorskilabs.lol_e_sports.model.tournamentModel

import com.jendorskilabs.lol_e_sports.model.playerModel.Player
import com.jendorskilabs.lol_e_sports.model.teamsModel.Teams
import org.springframework.data.annotation.Id
import org.springframework.data.annotation.TypeAlias
import javax.validation.constraints.NotEmpty

/**
 * Model for Tournaments, should contain other information over time.
 *
 * the LeagueId may not be present in every case
 *
 * @Id needed for Tournament
 * */
@TypeAlias("tournament")
data class Tournament(@Id val tournamentID: String,
                      val endDate: String = "", // 2018-11-03
                      val slug: String = "", // world_championship_2018
                      val startDate: String = "", // 2018-10-01
                      val leagueID: String = ""
) {

        /**
         * This constructor is for the generic Tournament Model
         * */
        constructor(tournamentID: String,
                             slug: String,
                    @NotEmpty(message = "The start Date must not be empty") startDate: String,
                    @NotEmpty(message = "The end Date must not be empty") endDate: String
        ):this(tournamentID)

    /**
         * This is for the stats of the best and worst Players, best and worst Teams,
         * of course some more people may like to share the podium with them, in
         * which ever case.
         * */
        constructor(tournamentID: String,
                    slug: String,
                    startDate: String,
                    endDate: String,
                    leagueID: String,
                    bestPlayersStats:List<Player>,
                    worstPlayersStats:List<Player>,
                    bestTeamsStats: List<Teams>,
                    worstTeamsStats: List<Teams>
        ): this(tournamentID)

    /**
         * This constructor is for the tournament object from the
         * event details from a matchID,
         * */
        constructor(tournamentID: String,
        tournamentLeagueID: String,
                    tournamentLeagueSlug:String,
                    tournamentLeagueImageURL: String,
        matchID:String,
        matchCount:Long):this(tournamentID)

}