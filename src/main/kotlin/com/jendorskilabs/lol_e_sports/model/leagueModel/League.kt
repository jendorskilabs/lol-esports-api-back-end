package com.jendorskilabs.lol_e_sports.model.leagueModel

import org.springframework.data.annotation.Id
import org.springframework.data.annotation.TypeAlias
import javax.validation.constraints.NotEmpty

/**
 * So this is the model, i added a constraint example, to help with validation.
 * Very easy, isnt it?
 *
 * on MongoDB, it saves it as a collection named league,
 * the TypeAlias annotation coming to play here,
 *
 * whilst the @property @Document is not compulsory, when you dont add it, the @Indexed will not work correctly
 *
 *  This model imitates LeagueModel.
 *
 *  Adding @Id ensures that if repetitive data of the same _id is added, it automatically replaces what was there before
 * */
@TypeAlias("league")
//@Document
data class League(
    @Id
    //@Indexed(unique = true)
    val id: String, // 100695891328981122
    val image: String = "", // http://static.lolesports.com/leagues/1585044513499_EM_BUG_2020%20(1).png
    val name: String = "", // European Masters
    val priority: Int ?= null, // 2
    val region: String ?= null, // EUROPE
    @NotEmpty(message = "Please provide a slug")
    val slug: String = ""// european-masters
)
