package com.jendorskilabs.lol_e_sports.controllers.errorControllers

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.boot.autoconfigure.web.servlet.error.AbstractErrorController
import org.springframework.boot.web.servlet.error.ErrorAttributes
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import javax.servlet.http.HttpServletRequest

@ConditionalOnProperty(name = ["errors.controller"], havingValue = "true")
@RestController
@RequestMapping("/error")
open class ErrorController(errorAttributes: ErrorAttributes?) : AbstractErrorController(errorAttributes, emptyList()) {

    val ERROR_PATH = "/error"

    @RequestMapping
    fun error(request: HttpServletRequest): ResponseEntity<MutableMap<String, Any>>{
        val body: MutableMap<String, Any> = getErrorAttributes(request, false)
        val status: HttpStatus = getStatus(request)

        return ResponseEntity<MutableMap<String, Any>>(body, status)
    }

    override fun getErrorPath(): String {
        return ERROR_PATH
    }

}