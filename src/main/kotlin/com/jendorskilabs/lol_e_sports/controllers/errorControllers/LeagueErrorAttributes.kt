package com.jendorskilabs.lol_e_sports.controllers.errorControllers

import com.jendorskilabs.lol_e_sports.model.errorModel.DefaultAppError
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.ModelAndView
import java.time.Instant
import java.time.format.DateTimeFormatter
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class LeagueErrorAttributes(private val currentApiVersion: String,
                            private val sendReportUri: String, private val timestamp: String = DateTimeFormatter.ISO_INSTANT.format(Instant.now())) : DefaultErrorAttributes() {

    /**
     * private final String currentApiVersion;
    private final String sendReportUri;
     * */

    override fun getErrorAttributes(webRequest: WebRequest?, includeStackTrace: Boolean): Map<String, Any> {
        val map:Map<String?, Any> = super.getErrorAttributes(webRequest, false)
        val defaultAppError: DefaultAppError = DefaultAppError.fromDefaultAttributeMap(
                currentApiVersion, map, sendReportUri, timestamp
        )
        return defaultAppError.toAttributeMap()
    }

    override fun resolveException(request: HttpServletRequest, response: HttpServletResponse, handler: Any?, ex: Exception): ModelAndView? {
        response.addHeader("LOLA-E-SPORTS", "THIS IS COOL")
        return super.resolveException(request, response, handler, ex)
    }


/*
    override fun getError(webRequest: WebRequest?): Throwable {
        return super.getError(webRequest)
    }

    override fun resolveException(request: HttpServletRequest, response: HttpServletResponse, handler: Any?, ex: Exception): ModelAndView? {
        return super.resolveException(request, response, handler, ex)
    }

    override fun getOrder(): Int {
        return super.getOrder()
    }
*/

}
