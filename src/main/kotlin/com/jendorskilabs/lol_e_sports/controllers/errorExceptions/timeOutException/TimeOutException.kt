package com.jendorskilabs.lol_e_sports.controllers.errorExceptions.timeOutException

import com.jendorskilabs.lol_e_sports.model.errorModel.ErrorCode

open class TimeOutException(message:String) : Throwable(message), ErrorCode {

    override fun getErrorCode(): String {
        return "Operation timed out, try again"
    }

}