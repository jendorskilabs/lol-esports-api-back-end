package com.jendorskilabs.lol_e_sports.controllers.errorExceptions.duplicateException

class DuplicatesException(s: String) : Throwable()
