package com.jendorskilabs.lol_e_sports.controllers.errorExceptions

import com.jendorskilabs.lol_e_sports.model.errorModel.ErrorCode
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

/**This exception indicates a not existing information from the database
 *
 * I have added the Annotation Response Status here, i hope it accepts it here
 * */
@ResponseStatus(code = HttpStatus.NOT_FOUND)
open class NonExistingException(message: String) : Exception(message), ErrorCode {

    override fun getErrorCode(): String {
        return "NE-001"
    }

}