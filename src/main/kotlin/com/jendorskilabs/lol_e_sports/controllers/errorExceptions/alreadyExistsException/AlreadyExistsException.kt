package com.jendorskilabs.lol_e_sports.controllers.errorExceptions.alreadyExistsException

import com.jendorskilabs.lol_e_sports.model.errorModel.ErrorCode

open class AlreadyExistsException(message:String) : Exception(message), ErrorCode{

    override fun getErrorCode(): String {
        return "Conflict, already exists"
    }

}