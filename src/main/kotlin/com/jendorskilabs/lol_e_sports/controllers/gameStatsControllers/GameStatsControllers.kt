package com.jendorskilabs.lol_e_sports.controllers.gameStatsControllers

import com.jendorskilabs.lol_e_sports.model.baseModel.BaseResponseModel
import com.jendorskilabs.lol_e_sports.model.matchModel.statsModel.MatchStatsModel
import com.jendorskilabs.lol_e_sports.services.gameStatsService.GameStatsService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

/**
 * We should be to provide information, such that we can
 * give stats for the profile of a team,
 *
 * */
@RestController
class GameStatsControllers {

    @Autowired
    lateinit var gameStatsService: GameStatsService

    @PostMapping("/v1/gamestats")
    fun gameStats(@Valid @RequestBody gameStats: List<MatchStatsModel>):BaseResponseModel<String>{
         val i = gameStatsService.insertGameStats(gameStats)

        return BaseResponseModel(HttpStatus.CREATED.value(), response = "Saved successfully. The size of the games stats is: $i")
    }

    /**
     * We will search for the team and return ALL stats that
     * have the teamName, it is going to be a long list
     * */
    @GetMapping("/v1/gamestats/teamstats/{teamName}")
    fun getTeamStatsForATeam(@PathVariable("teamName")
                             @Valid teamName:String):BaseResponseModel<List<MatchStatsModel>>{

       val teamStats = gameStatsService.getGameStatsForTeamName(teamName)

        return BaseResponseModel(HttpStatus.OK.value(), response = teamStats)
    }

    /**
     * We will search for the player and return ALL stats
     * that has the playerName, another very long list.
     * This MUST BE PAGINATED, no other option.
     * 25 items at a time, fixed.
     */
    @GetMapping("/v1/gamestats/playerstats/{playerName}")
    fun getPlayerStatsForAPlayer(@PathVariable("playerName") playerName: String):BaseResponseModel<List<MatchStatsModel>>{
        val playerStats = gameStatsService.getGameStatsForPlayerName(playerName)

        return BaseResponseModel(HttpStatus.OK.value(), response = playerStats)
    }

    /**
     * Get all the player stats for a tournament,
     * This is too much, it will slow down performance
     * We can use pagination here.
     *
     * If we are getting player stats for a tournament, thats going to be a lot of stats.
     * Pagination is a must
     * */
    @GetMapping("/v1/gamestats/playerstats/{playerName}/tournament/{tournamentID}/{pageSize}/{pageLength}")
    fun getPlayerStatsForATournament(@PathVariable("playerName", required = true) playerName: String,
                                    @PathVariable("tournamentID", required = true) tournamentID: String,
                                    @PathVariable("pageSize", required = true)pageSize:Int,
                                     @PathVariable("pageLength", required = true)pageLength:Int):BaseResponseModel<List<MatchStatsModel>>{

        TODO()

    }

    /**
     * Get the stats for a player in a game
     * */
    @GetMapping("/v1/gamestats/playerstats/{playerName}/game/{gameID}")
    fun getPlayerStatsForAGame(@PathVariable("playerName", required = true) playerName: String,
                                @PathVariable("gameID", required = true) gameID:String):BaseResponseModel<MatchStatsModel>{
        TODO()
    }
}