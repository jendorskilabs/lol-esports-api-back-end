package com.jendorskilabs.lol_e_sports.controllers.leagueControllers

import com.jendorskilabs.lol_e_sports.model.baseModel.BaseResponseModel
import com.jendorskilabs.lol_e_sports.model.leagueModel.League
import com.jendorskilabs.lol_e_sports.services.leagueServices.LeagueService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
class LeagueControllers{

    @Autowired
    lateinit var leagueService: LeagueService

    @GetMapping("/v1/leagues")
    fun getLeagues() : BaseResponseModel<List<League>> {

        return leagueService.leagues()
    }
    @GetMapping("/v1/leagues/{id}")
    fun getALeague(@PathVariable id: String) : BaseResponseModel<League> = leagueService.aLeague(id)

    @PostMapping("/v1/aleague")
    fun postALeague(@Valid @RequestBody league: League):BaseResponseModel<League>? {

        val l = league.region?.let { league.priority?.let { it1 -> leagueService.aLeague(league.id,league.slug, league.name, it, league.image, it1) } }

        return l//"${league.name} now is saved. Its priority is ${league.priority}"
    }

    @PostMapping("/v1/leagues")
    fun postLeagues(@Valid @RequestBody leagueList: List<League>): BaseResponseModel<String>{
        val l = leagueService.leagues(leagueList)

        return l
    }

    @PutMapping("/v1/leagues/{id}")
    fun updateLeagues(@PathVariable id:String, @RequestBody league: League){
        assert(league.id == id)

        leagueService.aLeague(id)
    }

}