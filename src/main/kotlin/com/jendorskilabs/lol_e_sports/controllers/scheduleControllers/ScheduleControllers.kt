package com.jendorskilabs.lol_e_sports.controllers.scheduleControllers

import org.springframework.web.bind.annotation.RestController

/**
 * Schedule of how each league or tournament will look like
 *
 * So we can have schedules for a league,
 * Schedule for a tournament,
 * Schedule for a team.
 *
 * We can also have completed schedules. information from here
 * */
@RestController
class ScheduleControllers