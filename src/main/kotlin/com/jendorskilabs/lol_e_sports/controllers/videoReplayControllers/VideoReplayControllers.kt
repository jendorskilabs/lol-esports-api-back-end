package com.jendorskilabs.lol_e_sports.controllers.videoReplayControllers

import com.jendorskilabs.lol_e_sports.model.baseModel.BaseResponseModel
import com.jendorskilabs.lol_e_sports.model.videoModel.Video
import com.jendorskilabs.lol_e_sports.services.videoServices.VideoService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@RestController
@Validated
class VideoReplayControllers {

    @Autowired
    lateinit var videoService: VideoService

    @PostMapping()
    fun insertVideoHighlights(@RequestBody @Valid videoList:List<Video>):BaseResponseModel<String>{

        TODO()
    }

    @GetMapping()
    fun getAllVideoHighlights():BaseResponseModel<List<Video>>{
        TODO()
    }

}