package com.jendorskilabs.lol_e_sports.controllers.playerControllers

import com.jendorskilabs.lol_e_sports.model.baseModel.BaseResponseModel
import com.jendorskilabs.lol_e_sports.model.playerModel.Player
import com.jendorskilabs.lol_e_sports.services.playerService.PlayerService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
class PlayerControllers{

    //@Autowired lateinit var leagueService: LeagueService

    //@Autowired lateinit var tournamentService: TournamentService

    //@Autowired lateinit var matchService: MatchService

    @Autowired
    lateinit var playerService: PlayerService

    /**
     * So i added a ResponseStatus, i hope it gets overridden
     * when there is an error
     * */
    @PostMapping("/v1/players", consumes = arrayOf(MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE))
    @ResponseStatus(value = HttpStatus.CREATED)
    fun insertPlayers(@RequestBody @Valid players:List<Player>):BaseResponseModel<String>{
        val i = playerService.insertAllPlayers(players)

        return BaseResponseModel(HttpStatus.CREATED.value(), response = i)
    }

    @PostMapping("/v1/addaplayer")
    @ResponseStatus(value = HttpStatus.CREATED)
    fun insertAPlayer(@RequestBody @Valid player:Player):BaseResponseModel<Player>{
        val p = playerService.addAPlayer(player)

        return BaseResponseModel(HttpStatus.ACCEPTED.value(), response = p)
    }

    @GetMapping("/v1/players")
    @ResponseStatus(HttpStatus.OK)
    fun getAllPlayers(@PathVariable("page")page:Int,
    @PathVariable("size") size:Int):BaseResponseModel<List<Player>>{

        val players:List<Player> = playerService.getAllPlayers()

        return BaseResponseModel(HttpStatus.OK.value(), response = players)
    }

    @PutMapping("/v1/updateaplayer")
    fun updateAPlayer(@PathVariable playerID:String, @RequestBody @Valid player:Player):BaseResponseModel<Player>{
        val p = playerService.updateAPlayer(playerID,player)

        return BaseResponseModel(HttpStatus.ACCEPTED.value(), response = p)
    }

    /**
     * How do we get the player with most kills in a league
     *
     * So we create a PlayerRecords and use this to deduce the players most kills in a league
     *
     * So we are getting the Player who has the most kills in a league, in descending order of most kills
     *
     * Something like getting the list of players with most goals in the Premier League
     *
     * */
    @GetMapping("/v1/player/mostkills/league")
    @ResponseStatus(HttpStatus.OK)
    fun getPlayerWithMostKillsInALeagueAllTime(@RequestParam(value = "leagueID" , required = true) leagueID:String):BaseResponseModel<Player>{
        TODO()
    }

    /**
     * So we are getting the player most kills for a tournament
     * */
    @GetMapping("/v1/player/mostkills/tournament")
    @ResponseStatus(HttpStatus.OK)
    fun getPlayerWithMostKillsInATournament(@RequestParam(value = "tournamentID", required =  true) tournamentID:String):BaseResponseModel<Player>{
        TODO()
    }

    @GetMapping("/v1/player/mostkills/team")
    @ResponseStatus(HttpStatus.OK)
    fun getPlayerWithMostKillsInATeamAllTime(@RequestParam(value = "teamSlug", required = false) team_slug:String,
                                      @RequestParam(value = "teamName", required = false) teamName:String,
                                      @RequestParam(value = "teamID", required = false) teamID:String):BaseResponseModel<Player>{
        TODO()
    }

    @DeleteMapping("/v1/player/deletePlayer")
    @ResponseStatus(HttpStatus.ACCEPTED)
    fun deletePlayer(@RequestBody @Valid player: Player):BaseResponseModel<Void>{
        val v =  playerService.deleteAPlayer(player)

        return BaseResponseModel(HttpStatus.ACCEPTED.value(),response = v)
    }

}