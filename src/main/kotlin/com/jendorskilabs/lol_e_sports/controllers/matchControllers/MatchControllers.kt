package com.jendorskilabs.lol_e_sports.controllers.matchControllers

import com.jendorskilabs.lol_e_sports.model.baseModel.BaseResponseModel
import com.jendorskilabs.lol_e_sports.model.baseModel.PaginatedBaseResponseModel
import com.jendorskilabs.lol_e_sports.model.matchModel.Match
import com.jendorskilabs.lol_e_sports.services.matchServices.MatchService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

/**
 * The Controllers for matches.
 *
 * Matches are different from Games.
 *
 * A match can consists of 5 games.
 * Each match is to have its matchID, the tournamentID which its being played, the leagueID too, preferably
 *
 * So we can add matches in a list
 * We can add a match
 * We can get a match
 * We can get all matches that has a particular tournamentID
 * We can get all matches that has a particular leagueID
 * We can delete a match
 * */
@RestController
class MatchControllers {

    @Autowired
    lateinit var matchService: MatchService

    /**
     * Working
     * */
    @PostMapping("/v1/matches")
    @ResponseStatus(HttpStatus.CREATED)
    fun insertMatches(@RequestBody @Valid matches:List<Match>):BaseResponseModel<String>{
        val i = matchService.insertAllMatches(matches)

        return BaseResponseModel(HttpStatus.CREATED.value(),response = "Successfully saved to the DB. Number of matches are $i")
    }

    /**
     * Working
     * */
    @PostMapping("/v1/amatch")
    @ResponseStatus(HttpStatus.CREATED)
    fun insertAMatch(@RequestBody @Valid match:Match):BaseResponseModel<String>{
        val i = matchService.insertAMatch(match)

        return BaseResponseModel(HttpStatus.CREATED.value(),response = "Successfully saved")
    }

    /**
     *Get all matches. I am returning a empty list*/
    @GetMapping("/v1/matches")
    @ResponseStatus(HttpStatus.OK)
    @Deprecated("So i may not be needing this")
    fun getAllMatches(@RequestParam page:Int, @RequestParam size:Int):PaginatedBaseResponseModel<List<Match>>{
        val math = matchService.getAllMatches(page, size)

        return PaginatedBaseResponseModel(HttpStatus.OK.value(), response = math,page = page,size = size)
    }

    /**
     * Get all matches for a league.
     *
     * Working
     */
    @GetMapping("/v1/matches/league/{leagueID}/page/{page}/size/{size}")
    @ResponseStatus(HttpStatus.OK)
    fun getMatchesForALeague(@PathVariable("leagueID") leagueID:String,
                             @PathVariable("page") page:Int,
                             @PathVariable("size")size:Int):PaginatedBaseResponseModel<List<Match>>{

        val t = matchService.getMatchesForLeague(leagueID,page,size)

        return PaginatedBaseResponseModel(HttpStatus.OK.value(),response = t.response, page = t.page,size = t.size)
    }

    /**
     * This endpoint is to get matches that have videos.
     *
     * So the [Match.hasVods] should be [true]
     *
     * Working as at 22-09-2020
     */
    @GetMapping("/v1/matches/hasVideos/page/{page}/size/{size}")
    @ResponseStatus(HttpStatus.OK)
    fun getMatchesWithVideos(@PathVariable("page") page:Int,
                            @PathVariable("size") size:Int):PaginatedBaseResponseModel<List<Match>>{

        val hasVideos = matchService.getMatchesThatHaveVideos(true, page, size)

        return PaginatedBaseResponseModel(HttpStatus.OK.value(), response = hasVideos.response,page = hasVideos.page, size = hasVideos.size)
    }

    /**
     * Update a match
     *
     * Working as at 23-09-2020
     * */
    @PutMapping("/v1/matches/updateamatch")
    @ResponseStatus(HttpStatus.ACCEPTED)
    fun updateAMatch(@RequestBody match: Match):BaseResponseModel<Match>{
        val newMatch = matchService.updateAMatch(match)

        return BaseResponseModel(HttpStatus.ACCEPTED.value(), response = newMatch)
    }

    /**Get matches by matchCount
     * or matchStrategyCount
     *
     * Working as at 24-09-2020
     * */
    @GetMapping("/v1/matches/matchCount/{matchCount}/page/{page}/size/{size}")
    @ResponseStatus(HttpStatus.OK)
    fun getMatchCount(@PathVariable("matchCount") matchCount:String, @PathVariable("page") page: Int, @PathVariable("size") size: Int):PaginatedBaseResponseModel<List<Match>>{
        val p = matchService.getMatchesByMatchCount(matchCount.toLong(), page,size)

        return PaginatedBaseResponseModel(HttpStatus.OK.value(),response = p.response,page = p.page,size = p.size)
    }

    /**
     * Get matches for a tournament
     *
     * and match count
     */
    @GetMapping("/v1/matches/tournament/{tournamentID}/matchCount/{matchCount}/page/{page}/size/{size}")
    @ResponseStatus(HttpStatus.OK)
    fun getMatchesForATournamentAndMatchCount(@PathVariable("tournamentID") tournamentID:String,
                                 @PathVariable("page") page: Int,
                                 @PathVariable("size") size: Int,
                                @PathVariable("matchCount") matchCount:Long):PaginatedBaseResponseModel<List<Match>>{
        val p = matchService.getMatchesByMatchCountAndTournament(matchCount, tournamentID, page, size)

        return PaginatedBaseResponseModel(HttpStatus.OK.value(),response = p.response, page = p.page, size = p.size)
    }

    /**
     * Delete a match
     *
     * Working as at 23-09-2020
     * */
    @DeleteMapping("/v1/matches/deleteamatch/{matchID}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun deleteAMatch(@Valid @PathVariable("matchID") matchID:String):BaseResponseModel<Unit>{
        val v = matchService.deleteAMatch(matchID)

        return BaseResponseModel(HttpStatus.NO_CONTENT.value(),response = v)
    }

}