package com.jendorskilabs.lol_e_sports.controllers.teamControllers

import com.jendorskilabs.lol_e_sports.model.baseModel.BaseResponseModel
import com.jendorskilabs.lol_e_sports.model.teamsModel.Teams
import com.jendorskilabs.lol_e_sports.services.teamServices.TeamService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.net.URI
import java.util.*
import javax.validation.Valid

@RestController
class TeamControllers {

    @Autowired
    lateinit var teamService: TeamService

    lateinit var responseHeaders: HttpHeaders

    @GetMapping("/v1/teams")
    fun getAllTeams():BaseResponseModel<List<Teams>>{
        val list:List<Teams> = teamService.getAllTeams()

        return BaseResponseModel(HttpStatus.OK.value(), response = list)
    }

    @PostMapping("/v1/teams")
    fun insertAllTeams(@RequestBody @Valid teamsList:List<Teams>):BaseResponseModel<String>{
        val string:String = teamService.insertAllTeams(teamsList)

        responseHeaders = HttpHeaders()
        responseHeaders.set("LOLA-X-API-HEADER","THE HEADER VALUE")
        responseHeaders.set("Time request was made", Date().toString())

        return BaseResponseModel(HttpStatus.OK.value(), response = string)
    }

    @GetMapping("/v1/teams/{leagueID}")
    fun getTeamsByLeagueID(@PathVariable @Valid leagueID:String):BaseResponseModel<List<Teams>>{

        val list:List<Teams> = teamService.getTeamsForALeague(leagueID)

        responseHeaders = HttpHeaders()
        responseHeaders.set("LOLA-X-API-HEADER","THE HEADER VALUE")
        responseHeaders.set("Time request was made", Date().toString())

        ResponseEntity.created(URI.create(""))
                 .header("MyResponseHeader", "MyValue")
                 .body("Hello World");

        return BaseResponseModel(HttpStatus.OK.value(), response = list)
    }

    @GetMapping("/v1/teams/{tournamentID}/{matchID}")
    fun getTeamsByTournamentIDAndMatchID(@PathVariable tournamentID:String, @PathVariable matchID:String):BaseResponseModel<List<Teams>>{

        val list:List<Teams> = teamService.getTeamsForAMatch(tournamentID, matchID)

        responseHeaders = HttpHeaders()
        responseHeaders.set("LOLA-X-API-HEADER","THE HEADER VALUE")
        responseHeaders.set("Time request was made", Date().toString())

        return BaseResponseModel(HttpStatus.OK.value(), response = list)
    }

}