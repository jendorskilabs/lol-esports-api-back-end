package com.jendorskilabs.lol_e_sports.controllers.errorConfiguration

import com.jendorskilabs.lol_e_sports.controllers.errorControllers.LeagueErrorAttributes
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes
import org.springframework.boot.web.servlet.error.ErrorAttributes
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@ConditionalOnProperty(name = ["errors.attributes"], havingValue = "true")
@Configuration
open class WebErrorConfiguration {

    @Value("\${api.version}")
    lateinit var currentApiVersion: String

    @Value("\${sendreport.uri}")
    lateinit var sendReportUri: String


    /**
     * We override the default [DefaultErrorAttributes]
     *
     * @return A custom implementation of ErrorAttributes
     */
    @Bean
    fun errorAttributes(): ErrorAttributes? {
        return LeagueErrorAttributes(currentApiVersion, sendReportUri)
    }


}