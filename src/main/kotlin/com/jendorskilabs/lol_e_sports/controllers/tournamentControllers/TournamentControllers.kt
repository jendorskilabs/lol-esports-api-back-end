package com.jendorskilabs.lol_e_sports.controllers.tournamentControllers

import com.jendorskilabs.lol_e_sports.model.baseModel.BaseResponseModel
import com.jendorskilabs.lol_e_sports.model.tournamentModel.Tournament
import com.jendorskilabs.lol_e_sports.services.tournamentServices.TournamentService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
class TournamentControllers {

    @Autowired
    lateinit var tournamentService: TournamentService

    @PostMapping("/v1/tournaments")
    fun insertAllTournaments(@RequestBody @Valid tournamentLists:List<Tournament>):BaseResponseModel<String>{
        val list:String = tournamentService.insertTournament(tournamentLists)

        return BaseResponseModel(HttpStatus.CREATED.value(), response = list)
    }

    @GetMapping("/v1/tournaments")
    fun getAllTournaments():BaseResponseModel<List<Tournament>>{
        val list:List<Tournament> = tournamentService.getAllTournaments()

        return BaseResponseModel(HttpStatus.OK.value(), response = list)
    }

    /**
     * Here the tournamentID is needed so we can get the tournament we want to update,
     * then we take the tournament in the request body, and update accordingly*/
    @PutMapping("/v1/updatetournament/{tournamentID}")
    fun updateATournament(@PathVariable(required = true) tournamentID:String, @RequestBody @Valid tournament: Tournament):BaseResponseModel<Tournament>{
        var t = tournamentService.updateATournament(tournamentID, tournament)

        return BaseResponseModel(HttpStatus.NO_CONTENT.value(), response = t)

    }

}