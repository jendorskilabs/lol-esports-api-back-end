package com.jendorskilabs.lol_e_sports.repository.gameStatsRepository

import com.jendorskilabs.lol_e_sports.model.matchModel.statsModel.MatchStatsModel
import org.springframework.data.domain.Pageable
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import reactor.core.publisher.Flux

/**We are supposed to use Rx here.
 *
 * Flux is the way
 * */
interface GameStatsRepository : ReactiveMongoRepository<MatchStatsModel, String> {

    //fun findBySummonerID(summonerID: String, pageable: Pageable): Flux<MatchStatsModel>

    fun findByPlayer(player: String, pageable: Pageable): Flux<MatchStatsModel>

    //fun findByTeamID(teamID: String, pageable: Pageable):Flux<MatchStatsModel>

    fun findByTeam(team: String, pageable: Pageable):Flux<MatchStatsModel>

}