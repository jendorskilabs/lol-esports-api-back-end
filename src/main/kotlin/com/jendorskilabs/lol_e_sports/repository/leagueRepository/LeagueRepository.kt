package com.jendorskilabs.lol_e_sports.repository.leagueRepository

import com.jendorskilabs.lol_e_sports.model.leagueModel.League
import org.springframework.data.mongodb.repository.MongoRepository
import java.util.*

/**
 * I am binding my code with {@link MongoRespository}
 *
 * import org.springframework.data.repository.CrudRepository
 *
 * It is worth noting that its not most of the methods here,
 * that will become endpoints.
 *
 * I was using CrudRepository before.
 * */
interface LeagueRepository : MongoRepository<League, String> {

    /**
     * Get ALL Leagues*/
    fun findBy() : List<League>

    /**
     * Get a league by its id
     * */
    fun findLeagueById(id: String): Optional<League>

    fun findLeagueByName(name:String): League

    //fun saveAll(leagueList: List<League>)

}