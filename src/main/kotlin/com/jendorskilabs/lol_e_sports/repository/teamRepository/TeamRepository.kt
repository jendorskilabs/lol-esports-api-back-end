package com.jendorskilabs.lol_e_sports.repository.teamRepository

import com.jendorskilabs.lol_e_sports.model.teamsModel.Teams
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import reactor.core.publisher.Flux

interface TeamRepository : ReactiveMongoRepository<Teams, String> {

    /**
     * Fetch ALL Teams, in ALL The LEAGUES
     * */
    //override fun findAll() : List<Teams>

    /**
     * Find a team via its Slug.
     * */
    fun findByTeamSlug(teamSlug: String): Flux<Teams>

    /**
     * Here we get team information,
     * based on the result
     * */
    //fun findByResult(): Teams

    /**
     * Get the Teams that played in a match
     * @param gameID the gameID needed to fetch the List of Teams
     * */
    //fun findByGameID(gameID: String): List<Teams>

    /**
     * Get the Teams that played in a match
     *
     * @param matchID the matchID needed to fetch the Teams that played.*/
    //fun findByMatchID(matchID: String): List<Teams>

    /**
     * Saves a Team
     * */
    //fun addTeam(@RequestBody team: Teams)

    /**
     * Saves a List of Teams
     * */
    //fun insertTeamsList(@RequestBody teamsList: List<Teams>)

    /**Get a team from its name*/
    fun findByTeamName(teamName:String): Flux<List<Teams>>

    /**Find by the id of the team
     * */
    fun findByTeamID(teamID: String):Flux<Teams>

}