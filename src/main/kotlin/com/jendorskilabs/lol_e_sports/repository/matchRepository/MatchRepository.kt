package com.jendorskilabs.lol_e_sports.repository.matchRepository

import com.jendorskilabs.lol_e_sports.model.matchModel.Match
import org.springframework.data.domain.Pageable
import org.springframework.data.mongodb.repository.Query
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import reactor.core.publisher.Flux
import javax.validation.Valid

/**The repository for the Match Model.
 *
 * Every information returned here must be wrapped in Flux
 *
 * Check the spring docs for supported Mongo Query methods
 * <url>https://docs.spring.io/spring-data/mongodb/docs/1.2.0.RELEASE/reference/html/mongo.repositories.html</url>
 *
 * So DO NOT use suspend functions here or any where you extend ReactiveMongoRepository, it leads to a BeanCreationException.
 *
 * */
interface MatchRepository : ReactiveMongoRepository<Match, String> {


    /**
     * Find all matches
     * */
    //fun findByMatchID()

    /**
     * Find all the Matches with a particular tournamentID
     * */
    fun findByTournamentIDOrderByEventStartTimeDesc(tournamentID:String, pageable: Pageable): Flux<Match>

    /**
     * Find all matches with a particular leagueID
     *
     * I still dont know how to use this:
     * @Query("{ 'leagueID' : ? }")
     * */
    fun findByLeagueID(@Valid leagueID: String, pageable: Pageable):Flux<Match>

    /**
     * Find all matches with a leagueName.
     *
     * this uses [Match.eventLeagueName]
     * */
    fun findByEventLeagueName(eventLeagueName:String, pageable: Pageable):Flux<Match>

    /**
     * Find all matches with a leagueName
     *
     * this uses [Match.leagueName]
     * */
    fun findByLeagueName(leagueName:String, pageable: Pageable):Flux<Match>

    /**
     * Find all matches with a league slug
     *
     * @return [Flux[Match]]
     * */
    fun findByEventLeagueSlug(eventLeagueSlug:String,pageable: Pageable): Flux<Match>

    /**
     * Find all matches that has videos (hasVods)
     *
     * I am not sure about this
     * */
    fun findByHasVodsIsTrue(hasVods:Boolean, pageable: Pageable):Flux<Match>

    /**
     *
     * Find all matches by the sectionName
     *
     */
    fun findBySectionName(sectionName:String, pageable: Pageable):Flux<Match>

    /**Find all matches by the stageName
     * */
    fun findByStageName(stageName:String, pageable: Pageable):Flux<Match>

    /**
     * Find all matches by the stageSlug
     * */
    fun findByStageSlug(stageSlug:String, pageable: Pageable):Flux<Match>

    /**Find all matches by the stageType
     * */
    fun findByStageType(stageType: String, pageable: Pageable):Flux<Match>

    /**
     * Find all matches by the type property
     * */
    fun findByType(type:String, pageable: Pageable):Flux<Match>

    /**Find all matches that are
     * */
    //fun findByEventStartLessThanOrEqualTime(eventStartTime:String)

    /**
     * Find ALL matches by the match count
     * */
    fun findByMatchStrategyCount(matchStrategyCount:Long, pageable: Pageable):Flux<Match>

    /**
     * Find AlL matches by the matchCount, same as above, but the fieldName is now different, facepalm:
     */
    fun findByMatchCountLike(matchCount:Long, pageable: Pageable):Flux<Match>

    /**Find all matches that has a match count for a tournament
     *
     * Supply the tournamentID, the matchCount
     *
     * Working as at 25-09-2020
     * */
    @Query(value = "{'tournamentID' : ?0 , 'matchCount' : ?1}")
    fun findByTournamentIDAndMatchCount(tournamentID:String, matchCount:Long, pageable: Pageable):Flux<Match>

    /**
     * Get by the tournamentID
     * */
    fun findByTournamentID(tournamentID: String, pageable: Pageable):Flux<Match>

    /**
     * Find all the matches, by the matchID
     *
     * You are not to use Sort parameter when you use pageable
     * */
    fun findByMatchCount(matchCount:Long, pageable: Pageable): Flux<Match>

    /**
     * Find all the matches, by the matchID
     * */
    @Query("{ 'matchID' : ?null }")
    fun findByMatchIDRegex(matchID: String): Flux<Match>

    /**
     * Find all the matches, by the matchID
     * */
    @Query("{ 'matchID' : ?0 }")
    fun findByMatchID(matchID: String): Flux<Match>

    /**Find matches by the match strategy types
     * */
    fun findByMatchStrategyType(matchStrategyType:String, pageable: Pageable): Flux<Match>

    /**
     * This stuff ain't working
     * */
    @Query(value="{'matchID' : null}", delete = true)
    fun deleteByMatchID(matchID: String)
}