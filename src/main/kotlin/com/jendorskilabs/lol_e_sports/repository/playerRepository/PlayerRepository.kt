package com.jendorskilabs.lol_e_sports.repository.playerRepository

import com.jendorskilabs.lol_e_sports.model.playerModel.Player
import org.springframework.data.domain.Pageable
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import reactor.core.publisher.Flux

/**
 * Repository for the Player
 *
 * We should be query the different stats for the player,
 * as well as the type of items, perks used.
 *
 *
 * */
interface PlayerRepository : ReactiveMongoRepository<Player, String> {

    /**
     * Find by the playerID
     * */
    fun findByPlayerID(playerID: String): Flux<Player>

    /**
     * Get players of a league
     **/
    fun findByLeagueID(leagueID: String, pageable: Pageable): Flux<Player>

    /**
     * Get players of a team,
     *
     * I am using Iterable, hopefully, to squeeze the most performance.
     *
     **/
    fun findByTeamSlug(teamSlug: String, pageable: Pageable): Flux<Player>

    /**
     * Get players by teamCode, also known as team slug.
     *
     * If i decide to remove this other properties, we wont be able to access them at all.
     * */
    fun findByTeamCode(teamCode: String, pageable: Pageable):Flux<Player>

    /**
     * Find Players by role
     * */
    fun findByRole(role: String, pageable: Pageable):Flux<Player>

    /**
     * find players by the teamID
     * */
    fun findByTeamID(teamID: String, pageable: Pageable):Flux<Player>

    /**
     * Get players via the league name
     **/
    fun findByHomeLeagueName(homeLeagueName: String, pageable: Pageable): Flux<Player>

    /**
     *
     */
    //fun findTeamsBestKDA(teamSlug: String):Player

    /**
     * Find the player with the worst K/D/A,
     * in a team.
     *
     * So what we will do here, is to take the teamSlug,
     * and use it as reference, any player with this teamSlug,
     * we fetch it and then get the player with the minimum K/D/A
     *
     * This method will fetch the worst KDA from all the stats we have in our DB.
     **/
    //fun findTeamsWorstKDAEver(teamSlug: String): Player

    /**
     * Find a player by summonerName.
     *
     * Regex is in use here.
     * */
    fun findBySummonerNameRegex(summonerName: String): Flux<Player>

}