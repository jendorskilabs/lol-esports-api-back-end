package com.jendorskilabs.lol_e_sports.repository.videoRepository

import com.jendorskilabs.lol_e_sports.model.videoModel.Video
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import reactor.core.publisher.Flux

/**
 * Repository for CRUDing videos
 *
 * A video should contain the watchLink, the gameID,
 * the matchID (if possible), tournamentID,
 * leagueID (if possible), teamSlug(if possible)
 *
 * And the playerIds that played in the game, in the video,
 * but the gameID, will help with that.
 */
interface VideoRepository : ReactiveMongoRepository<Video, String> {

    /**
     * Get the video for a game
     *
     * Note, we are getting the video vods,
     * it usually is from youtube, but it could be from other outlets
     *
     * Be warned.
     *
     * Maybe in the future, we will have our own media repository house,
     * that collects videos, images etc.
     * */
    fun getByGameID(gameID: String): Flux<Video>

    /**
     * Get video by the matchId
     * */
    fun getByMatchID(matchID: String): Flux<Video>

    /**
     * Get all the videos of a league,
     * by looking up its slug
     *
     * @param leagueSlug the League Slug identity needed, to fetch the videos in the DB
     * */
    fun getByLeagueSlug(leagueSlug: String) : Flux<List<Video>>

    /**
     * Get the videos of a player,
     * maybe, somehow, we have the videos of a player,
     * this way, we can look up a player up close.
     *
     * This is for future releases, work to be done in the future
     * */
    //fun getByPlayerName(playerName: String):List<Video>

    /**Get videos of a league, by the leagueName
     * */
    fun getByLeagueName(leagueName: String): Flux<List<Video>>

    /**
     * Get videos for a tournament via the tournamentID
     * */
    fun getByTournamentID(tournamentID: String): Flux<Video>

    /**
     * Same as leagueName
     * */
    fun getByEventLeagueName(eventLeagueName: String): Flux<List<Video>>

    /**
     * Insert a video
     * */
    //fun insertVideo(video: Video)

}