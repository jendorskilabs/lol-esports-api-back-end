package com.jendorskilabs.lol_e_sports.repository.tournamentRepository

import com.jendorskilabs.lol_e_sports.model.tournamentModel.Tournament
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import reactor.core.publisher.Flux

interface TournamentRepository : ReactiveMongoRepository<Tournament, String> {


    /**
     * Get all the tournaments, for a league
     * */
    fun findByLeagueID(leagueID: String): Flux<List<Tournament>>

    /**
     * Get a tournament from its tournamentID
     * */
    fun findByTournamentID(tournamentID: String): Flux<Tournament>

    /**
     * Get all tournaments
     * */
    //fun findAllTournament():List<Tournament>

}