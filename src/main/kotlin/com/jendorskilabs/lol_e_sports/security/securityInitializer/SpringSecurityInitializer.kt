package com.jendorskilabs.lol_e_sports.security.securityInitializer

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer

open class SpringSecurityInitializer : AbstractSecurityWebApplicationInitializer(){

}