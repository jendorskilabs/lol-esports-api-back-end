package com.jendorskilabs.lol_e_sports.security.securityAdapter

import com.jendorskilabs.lol_e_sports.security.PreAuthTokenHeaderFilter
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.PropertySource
import org.springframework.core.annotation.Order
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.web.access.ExceptionTranslationFilter
import org.springframework.security.web.authentication.Http403ForbiddenEntryPoint

/**
 * */
@Configuration
@EnableWebSecurity
@PropertySource("classpath:application.properties")
@Order(1)
@EnableGlobalMethodSecurity(prePostEnabled = true)
class AuthTokenSecurityConfig : WebSecurityConfigurerAdapter() {

    @Value("LoLA-HEADER-API-KEY")
    lateinit var authHeaderName: String

    @Value("123456")
    lateinit var authHeaderValue: String

    lateinit var filter: PreAuthTokenHeaderFilter

    lateinit var principals: String

    override fun configure(http: HttpSecurity?) {
        filter = PreAuthTokenHeaderFilter(authHeaderName)

        filter.setAuthenticationManager {
            it.apply {
                principals = it.principal as String

                if (authHeaderValue != principal) {
                    throw BadCredentialsException("Wrong Credentials")
                }
                it.isAuthenticated = true

            }
        }

        http?.antMatcher("/v1/**")
                //?.antMatcher("/v1/aleague")
                //?.antMatcher("/v1/leagues/{id}")
                //?.antMatcher("/v1/teams")
                //?.antMatcher("/v1/teams/{leagueID}")
                //?.antMatcher("/v1/teams/{tournamentID}/{matchID}")
                //?.antMatcher("")/**For more endpoints*/
                ?.csrf()
                ?.disable()
                ?.sessionManagement()
                ?.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                ?.and()
                ?.addFilter(filter)
                ?.addFilterBefore( ExceptionTranslationFilter(
                         Http403ForbiddenEntryPoint()),
                        filter::class.java
                )
                ?.authorizeRequests()
                ?.anyRequest()
                ?.authenticated()
    }
}