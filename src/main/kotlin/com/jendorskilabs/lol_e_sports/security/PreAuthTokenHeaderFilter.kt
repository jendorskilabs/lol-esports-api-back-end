package com.jendorskilabs.lol_e_sports.security

import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter
import javax.servlet.http.HttpServletRequest

open class PreAuthTokenHeaderFilter(authHeaderNames: String) : AbstractPreAuthenticatedProcessingFilter() {

    private val authHeaderName: String = authHeaderNames

    override fun getPreAuthenticatedCredentials(request: HttpServletRequest?): Any {
        return "N/A"
    }

    override fun getPreAuthenticatedPrincipal(request: HttpServletRequest?): Any? {
        return request?.getHeader(authHeaderName)
    }

}