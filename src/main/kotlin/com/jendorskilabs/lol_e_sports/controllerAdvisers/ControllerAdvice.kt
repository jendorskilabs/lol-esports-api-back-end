package com.jendorskilabs.lol_e_sports.controllerAdvisers

import com.jendorskilabs.lol_e_sports.controllers.errorExceptions.NonExistingException
import com.jendorskilabs.lol_e_sports.controllers.errorExceptions.alreadyExistsException.AlreadyExistsException
import com.jendorskilabs.lol_e_sports.controllers.errorExceptions.duplicateException.DuplicatesException
import com.jendorskilabs.lol_e_sports.controllers.errorExceptions.timeOutException.TimeOutException
import com.jendorskilabs.lol_e_sports.model.errorModel.SampleErrorModel
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.http.HttpStatus
import org.springframework.http.InvalidMediaTypeException
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestControllerAdvice
import java.net.UnknownHostException
import java.util.*
import javax.servlet.http.HttpServletRequest

/**
 * Here, we handle all and any type of exceptions
 * making sure that we send in the appropriate error response
 * */
@RestControllerAdvice
@ConditionalOnProperty(name = ["errors.controlleradvice"], havingValue = "true")
class ControllerAdvice  {

    @Value("\${sendreport.uri}")
    lateinit var sendReportUri: String

    @Value("\${api.version}")
    lateinit var currentApiVersion: String

    @ExceptionHandler(AlreadyExistsException::class)
    @ResponseStatus(HttpStatus.CONFLICT)
    fun handleAlreadyExisting(request: HttpServletRequest?, ex: AlreadyExistsException): ResponseEntity<SampleErrorModel>{
        val error = ex.getErrorCode().let {
            SampleErrorModel(HttpStatus.CONFLICT.value(),
            it, currentApiVersion)
        }
        return ResponseEntity(error, HttpStatus.CONFLICT)
    }

    @ExceptionHandler(DuplicatesException::class)
    @ResponseStatus(HttpStatus.CONFLICT)
    fun duplicates(request: HttpServletRequest?,
                   ex: DuplicatesException):ResponseEntity<SampleErrorModel>{
        return ResponseEntity<SampleErrorModel>(SampleErrorModel(HttpStatus.CONFLICT.value(),
                ex.message, currentApiVersion), HttpStatus.CONFLICT)
    }

    @ExceptionHandler(com.mongodb.MongoWriteException::class)
    @ResponseStatus(HttpStatus.CONFLICT)
    fun duplicateWriteError(request: HttpServletRequest?, ex: com.mongodb.MongoWriteException):ResponseEntity<SampleErrorModel>{
        return ResponseEntity<SampleErrorModel>(SampleErrorModel(HttpStatus.CONFLICT.value(),
        ex.message,currentApiVersion), HttpStatus.CONFLICT)
    }

    //UnknownHostException
    @ExceptionHandler(UnknownHostException::class)
    @ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
    fun unknownHost(request: HttpServletRequest?, ex: UnknownHostException):ResponseEntity<SampleErrorModel>{
        return ResponseEntity<SampleErrorModel>(SampleErrorModel(HttpStatus.SERVICE_UNAVAILABLE.value(),
        ex.message, currentApiVersion),HttpStatus.SERVICE_UNAVAILABLE)
    }

    @ExceptionHandler(NonExistingException::class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    fun handleNonExisting(request: HttpServletRequest?,
                          ex: NonExistingException): ResponseEntity<SampleErrorModel> {
        val error = ex.message?.let {
            SampleErrorModel(
                HttpStatus.NOT_FOUND.value(),
                    it,
                currentApiVersion)
        }
        return ResponseEntity(error, HttpStatus.NOT_FOUND)
    }

    @ExceptionHandler(EmptyResultDataAccessException::class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    fun handleThis():ResponseEntity<SampleErrorModel> {
        return ResponseEntity<SampleErrorModel>(SampleErrorModel(HttpStatus.NOT_FOUND.value(),
                "We could not find this stuff", currentApiVersion), HttpStatus.NOT_FOUND)
    }

    @ExceptionHandler(NoSuchElementException::class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    fun noSuchElementFound(e:NoSuchElementException):ResponseEntity<SampleErrorModel>{
        return ResponseEntity(SampleErrorModel(HttpStatus.NOT_FOUND.value(),
        e.message!!, currentApiVersion), HttpStatus.NOT_FOUND)
    }

    //@ExceptionHandler(RuntimeException::class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    fun runtimeHandler(e:java.lang.RuntimeException): ResponseEntity<SampleErrorModel>{
        return ResponseEntity<SampleErrorModel>(SampleErrorModel(HttpStatus.INTERNAL_SERVER_ERROR.value(),
        e.message!!, currentApiVersion), HttpStatus.INTERNAL_SERVER_ERROR)
    }

    @ExceptionHandler(IllegalArgumentException::class)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun illegalArgumentHandler(ex:java.lang.IllegalArgumentException):ResponseEntity<SampleErrorModel>{
        return ResponseEntity<SampleErrorModel>(SampleErrorModel(HttpStatus.NO_CONTENT.value(),
        ex.message,currentApiVersion),HttpStatus.NO_CONTENT)
    }

    @ExceptionHandler(InvalidMediaTypeException::class)
    @ResponseStatus(HttpStatus.UNSUPPORTED_MEDIA_TYPE)
    fun invalidMediaTypeHandler(ex:InvalidMediaTypeException):ResponseEntity<SampleErrorModel>{
        return ResponseEntity<SampleErrorModel>(SampleErrorModel(HttpStatus.UNSUPPORTED_MEDIA_TYPE.value(),
        ex.message, currentApiVersion), HttpStatus.UNSUPPORTED_MEDIA_TYPE)
    }

    @ExceptionHandler(TimeOutException::class)
    @ResponseStatus(HttpStatus.GATEWAY_TIMEOUT)
    fun timeOutException(ex:TimeOutException):ResponseEntity<SampleErrorModel>{
        return ResponseEntity<SampleErrorModel>(SampleErrorModel(HttpStatus.GATEWAY_TIMEOUT.value(),
        ex.message, currentApiVersion), HttpStatus.GATEWAY_TIMEOUT)
    }

    @ExceptionHandler(BadCredentialsException::class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    fun badCredentialsException(ex:BadCredentialsException):ResponseEntity<SampleErrorModel>{
        val code = HttpStatus.UNAUTHORIZED.value()
        return ResponseEntity(SampleErrorModel(code,ex.message, currentApiVersion), HttpStatus.UNAUTHORIZED)
    }

    @ExceptionHandler(ClassCastException::class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    fun classCastException(ex:ClassCastException):ResponseEntity<SampleErrorModel>{
        val code = HttpStatus.INTERNAL_SERVER_ERROR.value()
        return ResponseEntity(SampleErrorModel(code, ex.message, currentApiVersion), HttpStatus.INTERNAL_SERVER_ERROR)
    }

    @ExceptionHandler(IllegalStateException::class)
    @ResponseStatus(HttpStatus.REQUEST_TIMEOUT)
    fun illegalState(ex:java.lang.IllegalStateException):ResponseEntity<SampleErrorModel>{
        return ResponseEntity(SampleErrorModel(HttpStatus.REQUEST_TIMEOUT.value(), ex.message, currentApiVersion), HttpStatus.REQUEST_TIMEOUT)
    }



}