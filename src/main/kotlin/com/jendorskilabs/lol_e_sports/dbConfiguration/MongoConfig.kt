package com.jendorskilabs.lol_e_sports.dbConfiguration

import com.mongodb.ConnectionString
import com.mongodb.MongoClientURI
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.PropertySource
import org.springframework.core.env.Environment
import org.springframework.data.mongodb.MongoDbFactory
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.SimpleMongoClientDbFactory
import org.springframework.data.mongodb.core.SimpleMongoDbFactory

@Configuration
@PropertySource("classpath:application.properties")
class MongoConfig {

    @Autowired
    private val env: Environment? = null

    /**MongoClientURI()
     * this is the old way
     * return SimpleMongoDbFactory(MongoClientURI(env!!.getProperty("spring.data.mongodb.uri")!!))
     * */
    @Bean
    fun mongoDbFactory(): MongoDbFactory {
        return SimpleMongoClientDbFactory(ConnectionString(env!!.getProperty("spring.data.mongodb.uri")!!))
    }

    @Bean
    fun mongoTemplate(): MongoTemplate {
        return MongoTemplate(mongoDbFactory())
    }

}