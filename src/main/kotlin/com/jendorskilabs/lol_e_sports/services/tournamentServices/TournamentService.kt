package com.jendorskilabs.lol_e_sports.services.tournamentServices

import com.jendorskilabs.lol_e_sports.controllers.errorExceptions.NonExistingException
import com.jendorskilabs.lol_e_sports.model.tournamentModel.Tournament
import com.jendorskilabs.lol_e_sports.repository.tournamentRepository.TournamentRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.web.bind.annotation.RequestBody
import java.time.Duration
import javax.validation.Valid

interface TournamentService {

    fun insertTournament(@RequestBody @Valid tournamentList: List<Tournament>): String

    fun getAllTournaments():List<Tournament>

    fun getATournament(tournamentID: String):Tournament

    fun updateATournament(tournamentID: String, tournament: Tournament): Tournament

    fun determineTournamentExists(tournamentID:String): Boolean

}

@Service("tournamentService")
class TournamentServiceImpl: TournamentService{

    @Autowired
    lateinit var tournamentRepository: TournamentRepository

    override fun insertTournament(tournamentList: List<Tournament>): String {

        tournamentRepository.insert(tournamentList)

        return "Created and accepted successfully, tournament size is ${tournamentList.size}"
    }

    /**
     * I cannot figure out the Exception to throw here if i am unable to to get all tournaments
     * */
    override fun getAllTournaments(): List<Tournament> {
        val list = tournamentRepository.findAll()
                .collectList()
                .blockOptional(Duration.ofMillis(500))
                .orElseThrow { throw Exception() }

        return list
    }

    override fun getATournament(tournamentID:String): Tournament {
        val t = tournamentRepository.existsById(tournamentID)
                .blockOptional()
                .get()

            if(t){

                return tournamentRepository.findById(tournamentID)
                        .blockOptional(Duration.ofMillis(500))
                        .orElseThrow { throw NonExistingException("The tournament you are trying to access does not exist") }

            }else throw NonExistingException("The tournament you are trying to access does not exist")
        }

    /**
     * This method is to update a given tournament
     *
     * If the updating could not be done, it throws the exception below
     * @throws IllegalAccessException
     * */
    override fun updateATournament(tournamentID: String, tournament: Tournament): Tournament {
        if(determineTournamentExists(tournamentID)){

            return tournamentRepository.save(tournament)
                    .blockOptional(Duration.ofMillis(500))
                    .orElseThrow { throw IllegalAccessException("This operation could not be carried out.") }

        }else throw NonExistingException("This tournament does not exist. Contact developers for more information")
    }

    override fun determineTournamentExists(tournamentID: String): Boolean {
        return tournamentRepository.existsById(tournamentID)
                .blockOptional(Duration.ofMillis(500))
                .get()
    }

}
