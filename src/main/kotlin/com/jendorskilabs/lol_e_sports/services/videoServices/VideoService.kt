package com.jendorskilabs.lol_e_sports.services.videoServices

import com.jendorskilabs.lol_e_sports.model.videoModel.Video
import com.jendorskilabs.lol_e_sports.repository.videoRepository.VideoRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Example
import org.springframework.stereotype.Service
import javax.validation.Valid

interface VideoService {

    fun addAVideo(video: Video)

    fun addVideos(videos: List<Video>):Int

    fun getByGameID(gameID:String):Video

    fun getByLeagueName(leagueName: String):List<Video>

    fun getByTournamentID(tournamentID: String):List<Video>

    fun getByMatchID(matchID: String):Video

}

@Service("videoService")
class VideoServiceImpl: VideoService{

    @Autowired
    lateinit var videoRepo: VideoRepository

    /**
     * Also, run validation checks
     * */
    override fun addAVideo(@Valid video: Video) {
        TODO("Not yet implemented")
    }

    /**
     * We need to run validation checks on @param videos
     *
     * To be sure we are not accepting in the wrong stuff
     * */
    override fun addVideos(@Valid videos: List<Video>): Int {
        val e: Example<List<Video>> = Example.of(videos)
        videoRepo.insert(videos)

        return videos.size
    }

    override fun getByGameID(gameID: String): Video {
        TODO("Not yet implemented")
    }

    override fun getByLeagueName(leagueName: String): List<Video> {
        TODO("Not yet implemented")
    }

    override fun getByTournamentID(tournamentID: String): List<Video> {
        TODO("Not yet implemented")
    }

    override fun getByMatchID(matchID: String): Video {
        TODO("Not yet implemented")
    }

}
