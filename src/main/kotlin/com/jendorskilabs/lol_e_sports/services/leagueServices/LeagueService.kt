package com.jendorskilabs.lol_e_sports.services.leagueServices

import com.jendorskilabs.lol_e_sports.controllers.errorExceptions.NonExistingException
import com.jendorskilabs.lol_e_sports.model.baseModel.BaseResponseModel
import com.jendorskilabs.lol_e_sports.model.leagueModel.League
import com.jendorskilabs.lol_e_sports.repository.leagueRepository.LeagueRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service

interface LeagueService {

    fun leagues() : BaseResponseModel<List<League>>

    fun aLeague(leagueId: String): BaseResponseModel<League>

    fun aLeague(leagueId: String, slug: String, name: String, region: String, image: String, priority: Int): BaseResponseModel<League>

    fun leagues(leagueList: List<League>) : BaseResponseModel<String>

}

@Service("leagueService")
class LeagueServiceImpl : LeagueService {

    @Autowired
    lateinit var leagueRepository: LeagueRepository

    override fun leagues(): BaseResponseModel<List<League>> {
        val l: List<League> = leagueRepository.findBy()

        return BaseResponseModel(HttpStatus.OK.value(), response = l)
    }

    override fun leagues(leagueList: List<League>): BaseResponseModel<String> {
        leagueRepository.saveAll(leagueList)
        return BaseResponseModel(HttpStatus.CREATED.value(), response = "Saved successfully")
    }

    override fun aLeague(leagueId: String): BaseResponseModel<League> {
        if (leagueId.length > 18 || leagueId.length < 18) throw NonExistingException("Bad League ID")

        val league:League  = leagueRepository
                .findLeagueById(leagueId)
                .orElseThrow{throw NonExistingException("No such league")}
        //.findById(leagueId)
        //.orElseThrow { throw NonExistingLeagueException("This league Id is wrong") }
        //.get()
        //.orElse(League(leagueId))

        //leagueRepository.save(league)

        return BaseResponseModel(HttpStatus.OK.value(), response = league)
    }

    /**Save a League, with all the required information
     * */
    override fun aLeague(leagueId: String, slug: String, name: String, region: String, image: String, priority: Int): BaseResponseModel<League> {
        val league = leagueRepository.findById(leagueId)
                //.orElse(BaseResponseModel(404, "Not found"))

        if(league.isPresent){
            return BaseResponseModel(HttpStatus.NOT_ACCEPTABLE.value(), "This League is already present", response = league.get())
        }else{
            leagueRepository.save(League(leagueId, image, name, priority, region, slug))
            return BaseResponseModel(HttpStatus.ACCEPTED.value(), response = league.get())
        }
    }

    fun String.capitalize(): String {

        when (this.length) {

            0 -> return this

            1 -> return this.toUpperCase()

            else -> return this[0].toTitleCase() + this.substring(1).toLowerCase()

        }
    }
}