package com.jendorskilabs.lol_e_sports.services.playerService

import com.jendorskilabs.lol_e_sports.controllers.errorExceptions.NonExistingException
import com.jendorskilabs.lol_e_sports.controllers.errorExceptions.alreadyExistsException.AlreadyExistsException
import com.jendorskilabs.lol_e_sports.controllers.errorExceptions.timeOutException.TimeOutException
import com.jendorskilabs.lol_e_sports.model.baseModel.PaginatedResponseModel
import com.jendorskilabs.lol_e_sports.model.playerModel.Player
import com.jendorskilabs.lol_e_sports.repository.playerRepository.PlayerRepository
import com.jendorskilabs.lol_e_sports.utilities.constants.BLOCK_OPTIONAL_TIME_OUT
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Example
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Service
import org.springframework.web.bind.annotation.RequestBody
import reactor.kotlin.core.publisher.toMono
import java.time.Duration
import javax.validation.Valid

interface PlayerService {

    fun insertAllPlayers(@RequestBody @Valid playerList:List<Player>):String

    fun getAPlayer(playerID:String):Player

    fun getPlayerBySummonerName(summonerName:String): Player

    fun getPlayersForLeague(leagueID: String, page: Int, size: Int):PaginatedResponseModel<List<Player>>

    fun getPlayersForHomeLeague(homeLeagueName : String, page: Int, size: Int): PaginatedResponseModel<List<Player>>

    fun getPlayersByTeamSlug(teamSlug: String, page: Int, size: Int): PaginatedResponseModel<List<Player>>

    fun getPlayersByRole(role: String, page: Int, size: Int): PaginatedResponseModel<List<Player>>

    fun getAllPlayers():List<Player>

    fun updateAPlayer(playerId: String, player:Player): Player

    fun addAPlayer(player:Player):Player

    fun deleteAPlayer(player: Player):Void

}

@Service("playerService")
class PlayerServiceImpl: PlayerService{

    @Autowired
    lateinit var playerRepository: PlayerRepository

    override fun insertAllPlayers(playerList: List<Player>): String {

        val l = playerRepository
                .insert(playerList)
                .count()
                .blockOptional()
                .get()

        return "Saved successfully, Total players added: $l"
    }

    override fun getAPlayer(playerID: String): Player {
        return playerRepository.findByPlayerID(playerID)
                .toMono()
                .blockOptional(Duration.ofMillis(BLOCK_OPTIONAL_TIME_OUT))
                .orElseThrow { throw NonExistingException("No player found with $playerID") }
    }

    override fun getPlayerBySummonerName(summonerName: String): Player {
        return playerRepository.findBySummonerNameRegex(summonerName)
                .toMono()
                .blockOptional(Duration.ofMillis(BLOCK_OPTIONAL_TIME_OUT))
                .orElseThrow { throw NonExistingException("No player found with $summonerName") }
    }

    override fun getPlayersForLeague(leagueID: String, page: Int, size: Int): PaginatedResponseModel<List<Player>> {
        val t = playerRepository.findByLeagueID(leagueID = leagueID, pageable = PageRequest.of(page, size))
                .switchIfEmpty { throw NonExistingException("Players not found for the league with page $page") }
                .collectList()
                .blockOptional(Duration.ofMillis(BLOCK_OPTIONAL_TIME_OUT))
                .orElseThrow { throw TimeOutException("Operation timed out") }

        return PaginatedResponseModel(t, page, t.size)
    }

    override fun getPlayersForHomeLeague(homeLeagueName: String, page: Int, size: Int): PaginatedResponseModel<List<Player>> {
        val t =  playerRepository.findByHomeLeagueName(homeLeagueName, PageRequest.of(page, size))
                .switchIfEmpty { throw NonExistingException("Players not found for $homeLeagueName with page $") }
                .collectList()
                .blockOptional(Duration.ofMillis(BLOCK_OPTIONAL_TIME_OUT))
                .orElseThrow { throw NonExistingException("Players not found for $homeLeagueName") }

        return PaginatedResponseModel(t, page, t.size)
    }

    override fun getPlayersByTeamSlug(teamSlug: String, page:Int, size:Int): PaginatedResponseModel<List<Player>> {
        val t = playerRepository.findByTeamSlug(teamSlug, PageRequest.of(page, size))
                .switchIfEmpty { throw NonExistingException("Players not found for $teamSlug") }
                .collectList()
                .blockOptional(Duration.ofMillis(BLOCK_OPTIONAL_TIME_OUT))
                .orElseThrow { throw TimeOutException("Operation to find players for $teamSlug timed out") }

        return PaginatedResponseModel(t,page,t.size)
    }

    override fun getPlayersByRole(role: String, page: Int, size: Int): PaginatedResponseModel<List<Player>> {
        val t = playerRepository.findByRole(role, PageRequest.of(page, size))
                .switchIfEmpty { throw NonExistingException("Players not found for $role role, for page $page") }
                .collectList()
                .blockOptional(Duration.ofMillis(BLOCK_OPTIONAL_TIME_OUT))
                .orElseThrow { throw  TimeOutException("Operation timed out to get the player for $role")}

        return PaginatedResponseModel(t, page, t.size)
    }

    /**
     * Get all Players without a time out.
     *
     *
     * It does not listen to the time out exception at all.
     *
     * Its not paginated, so a timeout is added
     * */
    override fun getAllPlayers(): List<Player> {
        return playerRepository
                .findAll()
                .collectList()
                .blockOptional(Duration.ofMillis(BLOCK_OPTIONAL_TIME_OUT))
                .orElseThrow { throw TimeOutException("This operation timed out, try again") }
                //.findAll(PageRequest.of(0,100)).toList()
    }

    override fun updateAPlayer(playerId: String, player: Player): Player {
        val oldPlayer: Player = getAPlayer(playerId)

        oldPlayer.summonerName = player.summonerName
        oldPlayer.playerID = player.playerID
        oldPlayer.firstName = player.firstName
        oldPlayer.lastName = player.lastName
        oldPlayer.role = player.role
        oldPlayer.teamCode = player.teamCode
        oldPlayer.leagueID = player.leagueID
        oldPlayer.teamName = player.teamName
        oldPlayer.homeLeagueName = player.homeLeagueName
        oldPlayer.teamSlug = player.teamSlug
        oldPlayer.teamID = player.teamID

        //TODO complete the rest here

        playerRepository.insert(oldPlayer)
                .blockOptional()
                .orElseThrow { throw TimeOutException("Operation timed out") }

        return oldPlayer
    }

    /**
     * TODO Create a Constants file to let us know the standard time out we give for the blockOptional
     *
     * It is worth noting that the playerExample works with the exists, to return a Mono<Boolean>
     * */
    override fun addAPlayer(player: Player): Player {
        val playerExample: Example<Player> = Example.of(player)
        val exists = playerRepository.exists(playerExample)
                .blockOptional()
                .get()

        if(!exists){
            return playerRepository.insert(player)
                    .blockOptional(Duration.ofMillis(3000))
                    .orElseThrow { throw IllegalStateException("Unable to add this player") }
        }else throw AlreadyExistsException("This player already exists")

    }

    /**
     * First we check if the player exists*/
    override fun deleteAPlayer(player: Player): Void {
        val playerExample: Example<Player> = Example.of(player)
        val exists = playerRepository.exists(playerExample)
                .blockOptional()
                .get()

        if(exists){
            //delete player
            return playerRepository.delete(player)
                    .blockOptional()
                    .get()
        }else throw NonExistingException("Player ${player.summonerName} doesn't exists in our DB")
    }

}