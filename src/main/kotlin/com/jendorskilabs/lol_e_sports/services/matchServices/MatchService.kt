package com.jendorskilabs.lol_e_sports.services.matchServices

import com.jendorskilabs.lol_e_sports.controllers.errorExceptions.NonExistingException
import com.jendorskilabs.lol_e_sports.controllers.errorExceptions.alreadyExistsException.AlreadyExistsException
import com.jendorskilabs.lol_e_sports.controllers.errorExceptions.duplicateException.DuplicatesException
import com.jendorskilabs.lol_e_sports.model.baseModel.PaginatedResponseModel
import com.jendorskilabs.lol_e_sports.model.matchModel.Match
import com.jendorskilabs.lol_e_sports.repository.matchRepository.MatchRepository
import com.jendorskilabs.lol_e_sports.utilities.constants.BLOCK_OPTIONAL_TIME_OUT
import com.jendorskilabs.lol_e_sports.utilities.determineDuplicates
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono
import java.time.Duration
import java.util.*
import reactor.kotlin.core.publisher.switchIfEmpty as switchIfEmpty1

interface MatchService {

    /**
     * Insert All Matches in a List
     * */
    fun insertAllMatches(matches:List<Match>):Int

    /**
     * Adds a match.
     *
     * A suspend function
     * */
    fun insertAMatch(match: Match)

    /**Get all matches in the DB, this must be paginated
     * */
    fun getAllMatches(page:Int, size:Int): List<Match>

    /**So we can also get all matches for a league
     * This must be paginated.
     *
     * @param page is the page number, starting from 0 and upwards
     * @param size is the number of items requested for, starts from 1 upwards and if you say you want zero items, well...
     * */
    fun getMatchesForLeague(leagueID: String, page:Int, size:Int):PaginatedResponseModel<List<Match>>

    /**
     * Get Matches for a tournament
     *
     * First check the tournament via its ID, to be sure the tournament exists
     *
     * This must be paginated.
     * */
    fun getMatchesForATournament(tournamentID: String, page:Int, size:Int):PaginatedResponseModel<List<Match>>

    /**Get matches that has videos.
     *
     * This will only return matches with videos paramters
     * */
    fun getMatchesThatHaveVideos(hasVideos:Boolean, page:Int, size:Int):PaginatedResponseModel<List<Match>>

    /**Get a match information
     *
     * First check if the matchID is available for the match, else throw a NONExisiting Exception
     * */
    fun getAMatch(matchID:String):Match

    /**This is essentially a PUT operation
     *
     * Update the match information.
     *
     * First you check if the match is available then update accordingly
     * */
    fun updateAMatch(match: Match):Match

    /**Get the matches for a team.
     *
     * Check first if the team information is available
     * Then get the matches for that team.
     *
     * Be sure the teamID belongs to only one team
     * */
    fun getMatchesForATeam(teamID:String, page:Int, size:Int):List<Match>

    /**So we take the information of the player to be sure we have the player information
     * Then we match the player information with the team(s), if available, take the
     * teamID and get the matches for the team
     * */
    fun getMatchesForAPlayer(playerID:String, page:Int, size:Int):List<Match>

    /**
     * Get matches for a stage type
     * */
    fun getMatchesForAStageType(stageType:String, page: Int, size: Int):List<Match>

    /**
     * Get matches by section name
     * */
    fun getMatchesBySection(sectionName:String, page: Int, size:Int):List<Match>

    /**Get matches by a leagueSlug.*
     *
     * Also if the league slug, we can get the match by the leagueName.
     *
     * Perhaps we need a method in a separate class that returns the league name when you enter the league slug
     * and another method in the class that returns the league slug when you enter the league name
     */
    fun getMatchesByLeagueSlug(leagueSlug:String, page: Int,size: Int):List<Match>

    /**
     * Get match via the match strategy types.
     * The match strategy could be:
     * a. bestOf*/
    fun getMatchesByMatchStrategyTypes(matchStrategy: String, page: Int, size: Int):List<Match>

    /**Get matches by their counts
     * This means a match can consist of 5 games or 3 games or two games or one game.
     * */
    fun getMatchesByMatchCount(matchCount:Long, page: Int, size: Int):PaginatedResponseModel<List<Match>>

    /**Get matches by the match count for a tournament.
     *
     * For example if we have a tournamentID and we want to get the list of matches that have 5games, this endpoint is perfect
     * for it.
     * */
    fun getMatchesByMatchCountAndTournament(matchCount:Long, tournamentID:String, page: Int, size: Int):PaginatedResponseModel<List<Match>>

    /**
     * Get matches for a tournament
     *
     * @return [PaginatedResponseModel]
     * */
    fun getMatchesByTournament(tournamentID: String, page: Int, size: Int):PaginatedResponseModel<List<Match>>

    /**
     * Similar to [getMatchesByLeagueSlug]
     *
     * It uses eventLeagueSlug
     * */
    fun getMatchesForEventLeague(leagueSlug: String, page: Int, size: Int):List<Match>

    /**Delete a match
     * */
    fun deleteAMatch(matchID:String)

}

@Service("matchService")
class MatchServiceImpl: MatchService{

    @Autowired lateinit var matchRepository: MatchRepository

    //@Autowired lateinit var tournamentRepository: TournamentRepository

    //@Autowired lateinit var playerRepository: PlayerRepository

    //@Autowired lateinit var teamRepository: TeamRepository

    //@Autowired lateinit var leagueRepository: LeagueRepository

    /**
     * So we have used suspend here.
     *
     * This means we use coroutines here, or light-weight threads here.
     * I have removed the coroutines
     *
     * So i first determine if there are duplicates, i will create
     * a utility class, to be sure.
     *
     * If there are duplicates, throw an exception, we will not be accepting duplicates, thank you very much.
     *
     * Now what if the some of the matches in the List already exists in the DB?
     * WE need to find a way of checking each match in that list to be sure they do not exist in the DB.
     * */
    override fun insertAllMatches(matches: List<Match>): Int {

        val longer:Mono<Long>

        val duplicates = determineDuplicates(matches)

        if(duplicates){
            throw DuplicatesException("Duplicates exists for this list, check and try again")
        }else{
            val h = hashSetOf(matches).flatten()
           longer  = matchRepository.insert(h).count()//matches
        }

        return longer.blockOptional().get().toInt()
    }

    /**
     * */
    fun filterMatches(matches: List<Match>){
        CoroutineScope(Dispatchers.IO).launch {
            matches.asFlow()

        }

        flow<Match> {
            matches.forEach {
                val match:Match? = matchRepository.findByMatchIDRegex(it.matchID)
                        .blockLast()

                println("Old match ID: "+it.matchID )
                println("New matchID: " + match?.matchID)

                if (match?.matchID?.contentEquals(it.matchID)!!){
                    throw DuplicatesException("One or more of this matches exists in the DB, try again")
                }
                //emit(it)
            }
        }
    }

    /**
     * Insert a match
     *
     * Check first that we do not have that match
     *
     * If the match doesn't exist, add the match to the DB
     * */
    override fun insertAMatch(match: Match) {
        val oldMatch:Match = matchRepository.findByMatchIDRegex(match.matchID)
                .last()
                .blockOptional()
                .orElseThrow { throw NonExistingException("Match not found in our DB") }


        //val matchExample: Example<Match> = Example.of(match)

        //val matcher:Boolean = matchExample.matcher.isAllMatching

        if(!oldMatch.matchID.contentEquals(match.matchID)){
            matchRepository.insert(match)
                    .blockOptional(Duration.ofMillis(2500))
        }else throw AlreadyExistsException("Match already exists")
    }

    /**
     * Here, get ALL matches. i am not sure of this.
     *
     * Its a problematic issue jare
     *
     * I now return an empty list
     * */
    override fun getAllMatches(page: Int, size: Int): List<Match> {

        return Collections.emptyList()
    }

    /**First get the League information, to be sure the League exists
     *
     * What if the DB only contains one item
     *
     * Working as at 23-09-2020
     * */
    override fun getMatchesForLeague(leagueID: String, page: Int, size: Int): PaginatedResponseModel<List<Match>> {
        //val league: League = leagueRepository.findLeagueById(leagueID).orElseThrow{throw NonExistingException("No such League available")}

        //This is for finding if the matchID exists on the DB
/*
        matchRepository.findById(leagueID)
                .blockOptional()
                .orElseThrow { throw NonExistingException("No such item exists jare: $leagueID") }
*/

        val list = matchRepository
                .findByLeagueID(leagueID, PageRequest.of(page, size))
                .switchIfEmpty1 { throw NonExistingException("No matches for the league $leagueID") }
                .collectList()
                .blockOptional(Duration.ofMillis(BLOCK_OPTIONAL_TIME_OUT))
                .orElseThrow { throw NonExistingException("No matches found for the league: $leagueID" ) }

        if(list.size == 0){
            throw NonExistingException("No matches found for league: $leagueID or DB is empty")
        }

        return PaginatedResponseModel(list,page,list.size)
    }

    override fun getMatchesForATournament(tournamentID: String, page: Int, size: Int): PaginatedResponseModel<List<Match>> {
        val t =  matchRepository
                .findByTournamentIDOrderByEventStartTimeDesc(tournamentID, PageRequest.of(page, size))
                .collectList()
                .blockOptional(Duration.ofMillis(BLOCK_OPTIONAL_TIME_OUT))
                .orElseThrow { throw NonExistingException("No such matches found for such tournament") }

        return PaginatedResponseModel(t,page,t.size)
    }

    /**
     * Its could also throw TimeOut  Exception.
     *
     * But it throws NonExistingException
     *
     * Working as at 22-09-2020
     * */
    override fun getMatchesThatHaveVideos(hasVideos: Boolean, page: Int, size: Int): PaginatedResponseModel<List<Match>> {

                val t = matchRepository
                .findByHasVodsIsTrue(hasVideos,PageRequest.of(page, size))
                        .collectList()
                        .blockOptional(Duration.ofMillis(BLOCK_OPTIONAL_TIME_OUT))
                        .orElseThrow { throw NonExistingException("No matches with videos") }//The operation to get matches with videos

        return PaginatedResponseModel(t,page,t.size)
    }

    override fun getAMatch(matchID: String): Match {
        return matchRepository
                .findById(matchID)
                .blockOptional()
                .orElseThrow { throw NonExistingException("No such match found") }
    }

    /**
     * First, we check if the match is on the DB.
     *
     * If its available, we then set the new information, replacing it with the old.
     *
     * Working as at 23-09-2020
     * */
    override fun updateAMatch(match: Match): Match {
        var oldMatch:Match = matchRepository.findByMatchID(match.matchID)
                .toMono()
                .blockOptional()
                .orElseThrow { throw NonExistingException("This match is not on our DB, are you trying to POST a new match information?. Don't try to update what does not exist") }

        if(oldMatch.matchID.contentEquals(match.matchID)){
            oldMatch.matchID = match.matchID
            oldMatch.eventLeagueName = match.eventLeagueName
            oldMatch.hasVods = match.hasVods
            oldMatch.eventBlockName = match.eventBlockName
            oldMatch.eventStartTime = match.eventStartTime
            oldMatch.eventLeagueSlug = match.eventLeagueSlug
            oldMatch.eventBlockName = match.eventBlockName
            oldMatch.gameID = match.gameID
            oldMatch.eventLeagueSlug = match.eventLeagueSlug
            oldMatch.gameWins = match.gameWins
            oldMatch.leagueID = match.leagueID
            oldMatch.tournamentID = match.tournamentID
            oldMatch.leagueImageURL = match.leagueImageURL
            oldMatch.videoURL = match.videoURL
            oldMatch.matchCount = match.matchCount
            oldMatch.sectionName = match.sectionName
            oldMatch.stageName = match.stageName
            oldMatch.stageSlug = match.stageSlug
            oldMatch.stageType = match.stageType
            oldMatch.previousMatchIdString = match.previousMatchIdString
            oldMatch.matchType = match.matchType
            oldMatch.matchStrategyType = match.matchStrategyType
            oldMatch.matchStrategyCount = match.matchStrategyCount
            oldMatch.type = match.type

            return matchRepository.save(oldMatch)
                    .blockOptional()
                    .get()
        }else throw NonExistingException("No such match to update")
    }

    /**
     * This fetches ALL Matches for a team in the DB.
     *
     * This one is a bit tricky
     *
     * I will need to get the team information.
     *
     * */
    override fun getMatchesForATeam(teamID: String, page: Int, size: Int): List<Match> {
/*
        val team: Teams = teamRepository.findByTeamID(teamID)
                .toMono()
                .blockOptional(Duration.ofMillis(500))
                .orElseThrow { throw NonExistingException("No such team found") }

        val teamsList:List<Teams> = teamRepository.findByTeamName(team.teamName)
                .toMono()
                .blockOptional(Duration.ofMillis(500))
                .orElseGet { Collections.emptyList() }
*/

        //val tList: List<Tournament> = tournamentRepository.findAll()


        TODO()
    }

    /**
     * This is also tricky, we need the player information.
     * */
    override fun getMatchesForAPlayer(playerID: String, page: Int, size: Int): List<Match> {
        TODO("Not yet implemented")
    }

    /**
     * Get the matches for a stage type
     * */
    override fun getMatchesForAStageType(stageType: String, page: Int, size: Int): List<Match> {
        return matchRepository.findByStageType(stageType, PageRequest.of(page, size))
                .switchIfEmpty1 { throw NonExistingException("No matches for stage type $stageType") }
                .collectList()
                .blockOptional(Duration.ofMillis(BLOCK_OPTIONAL_TIME_OUT))
                .orElseThrow { throw NonExistingException("No matches for $stageType") }
    }

    /**
     * Get the matches by section
     * */
    override fun getMatchesBySection(sectionName: String, page: Int, size: Int): List<Match> {
        val t =  matchRepository.findBySectionName(sectionName, PageRequest.of(page,size))
                .switchIfEmpty1 { throw NonExistingException("No matches found for sections $sectionName") }
                .collectList()
                .blockOptional(Duration.ofMillis(BLOCK_OPTIONAL_TIME_OUT))
                .orElseThrow { throw NonExistingException("No such matches for $sectionName found") }

        if (t.size == 0){
            throw NonExistingException("No matches found for section $sectionName")
        }
        return t
    }

    /**
     * Get All Matches by League Slug
     *
     * TODO Get a utility class that returns the league name when you enter the leagueSlug
     * */
    override fun getMatchesByLeagueSlug(leagueSlug: String, page: Int, size: Int): List<Match> {
        //val likeLeague  = getMatchesForEventLeagueSingle(leagueSlug, page, size)

        val t = matchRepository.findByEventLeagueSlug(leagueSlug,PageRequest.of(page, size))
                .collectList()
                .blockOptional(Duration.ofMillis(BLOCK_OPTIONAL_TIME_OUT))
                .orElseThrow { throw NonExistingException("No such matches found for $leagueSlug found") }

        if (t.size == 0){
            throw NonExistingException("No matches for league $leagueSlug")
        }

        return t
    }

    /**
     * Get matches by the strategy types.
     *
     * Strategy types are:
     * */
    override fun getMatchesByMatchStrategyTypes(matchStrategy: String, page: Int, size: Int): List<Match> {

        val t = matchRepository.findByMatchStrategyType(matchStrategy,PageRequest.of(page,size))
                .collectList()
                .blockOptional(Duration.ofMillis(BLOCK_OPTIONAL_TIME_OUT))
                .orElseThrow{throw NonExistingException("No matches found for type of $matchStrategy")}

        if (t.size == 0){
            throw NonExistingException("No matches are found for this type $matchStrategy")
        }

        return t
    }

    /**
     * Get matches by their count, that is get matches that have 5 games for examples
     *
     *
     * We should be able to get matches that has 5 games, for example, for a team?? or for a tournament
     * */
    override fun getMatchesByMatchCount(matchCount: Long, page: Int, size: Int): PaginatedResponseModel<List<Match>> {

        val strategyCount = matchRepository.findByMatchStrategyCount(matchCount,PageRequest.of(page, size))
                .toMono()
                .blockOptional(Duration.ofMillis(BLOCK_OPTIONAL_TIME_OUT))
                .orElseThrow { throw NonExistingException("No such matches for match strategy counts $matchCount found") }

        val t = matchRepository
                .findByMatchCount(matchCount, PageRequest.of(page, size))
                .defaultIfEmpty(strategyCount)
                .collectList()
                .blockOptional(Duration.ofMillis(BLOCK_OPTIONAL_TIME_OUT))
                .orElseThrow { throw NonExistingException("No such matches for match counts $matchCount found") }

        if (t.size == 0){
            throw NonExistingException("No matches found for the match count $matchCount")
        }

        return PaginatedResponseModel(t, page, t.size)
    }

    /**
     * Get ALL Matches by the match count and the tournamentID.
     *
     * The tournamentID is important, so we can know the tournament we are
     * pulling this information for.
     *
     * We can pull matches that has 4 games for a tournament.
     * Its pageable and a list
     *
     * I also used the suspend functions, so it can use the light-weight threads, i have removed the suspend function
     *
     * Working as at 25-09-2020
     * */
    override fun getMatchesByMatchCountAndTournament(matchCount: Long, tournamentID: String, page: Int, size: Int): PaginatedResponseModel<List<Match>> {

        val t = matchRepository.findByTournamentIDAndMatchCount(tournamentID,matchCount,PageRequest.of(page, size))
                .switchIfEmpty1 { throw NonExistingException("No matches for the tournamentID: $tournamentID and match count $matchCount") }
                .collectList()
                .blockOptional(Duration.ofMillis(BLOCK_OPTIONAL_TIME_OUT))
                .orElseThrow { throw NonExistingException("No such matches found, or an error occurred") }

        return PaginatedResponseModel(t,page, t.size)
    }

    /**
     *Get Matches for a tournament
     * Working as at 23-09-2020
     * */
    override fun getMatchesByTournament(tournamentID: String, page: Int, size: Int): PaginatedResponseModel<List<Match>> {
        val t = matchRepository.findByTournamentID(tournamentID,PageRequest.of(page, size))
                .switchIfEmpty1 { throw NonExistingException("No matches found for the tournament") }
                .collectList()
                .blockOptional(Duration.ofMillis(BLOCK_OPTIONAL_TIME_OUT))
                .orElseThrow{throw NonExistingException("No such matches by the tournament")}

        return PaginatedResponseModel(t,page, t.size)
    }

    override fun getMatchesForEventLeague(leagueSlug: String, page: Int, size: Int):List<Match> {
        return matchRepository.findByEventLeagueName(leagueSlug, PageRequest.of(page, size))
                .collectList()
                .blockOptional(Duration.ofMillis(BLOCK_OPTIONAL_TIME_OUT))
                .orElseThrow { throw NonExistingException("No such matches for $leagueSlug found") }
    }

    /**
     * Delete a match
     *
     * Working as at 23-09-2020
     * */
    override fun deleteAMatch(matchID: String) {
        val oldMatch:Match = matchRepository.findByMatchID(matchID)
                .toMono()
                .blockOptional()
                .orElseThrow { throw NonExistingException("This match is not on our DB, are you trying to POST a new match information?. Don't try to delete what does not exist") }

        println("Match: " + oldMatch.matchID)

        matchRepository.delete(oldMatch).block()

    }

}
