package com.jendorskilabs.lol_e_sports.services.gameStatsService

import com.jendorskilabs.lol_e_sports.controllers.errorExceptions.NonExistingException
import com.jendorskilabs.lol_e_sports.controllers.errorExceptions.timeOutException.TimeOutException
import com.jendorskilabs.lol_e_sports.model.matchModel.statsModel.MatchStatsModel
import com.jendorskilabs.lol_e_sports.repository.gameStatsRepository.GameStatsRepository
import com.jendorskilabs.lol_e_sports.repository.matchRepository.MatchRepository
import com.jendorskilabs.lol_e_sports.repository.playerRepository.PlayerRepository
import com.jendorskilabs.lol_e_sports.repository.teamRepository.TeamRepository
import com.jendorskilabs.lol_e_sports.repository.tournamentRepository.TournamentRepository
import com.jendorskilabs.lol_e_sports.utilities.constants.BLOCK_OPTIONAL_TIME_OUT
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Service
import org.springframework.web.bind.annotation.RequestBody
import java.time.Duration

/**
 * You will notice the get methods are duplicated, this is because of pagination, in case the
 * user doesnt add a preferred pagination size.
 *
 * pageSize: this means the particular index of all the items. e.g. 0,1.
 *
 * pageLength: this means the total number of items, for all items, e.g. 20, 50, 100
 * */
interface GameStatsService {

    fun insertGameStats(gameStats: List<MatchStatsModel>):Int

    fun getGameStatsForPlayer(playerId: String):List<MatchStatsModel>

    fun getGameStatsForPlayer(playerId: String, pageSize:Int, pageLength:Int):List<MatchStatsModel>

    fun getGameStatsForTeam(teamID: String):List<MatchStatsModel>

    fun getGameStatsForTeam(teamID: String, page: Int, size: Int):List<MatchStatsModel>

    fun getGameStatsForPlayerName(playerName: String): List<MatchStatsModel>

    fun getGameStatsForPlayerName(playerName: String, page: Int, size: Int): List<MatchStatsModel>

    fun getGameStatsForTeamName(teamName: String):List<MatchStatsModel>

    fun getGameStatsForTeamName(teamName: String, page: Int, size: Int):List<MatchStatsModel>

    fun getPlayerGameStatsForTournament(playerName: String, tournamentID: String, page: Int, size: Int): List<MatchStatsModel>

}

@Service("gameStatsService")
class GameStatsServiceImpl : GameStatsService {

    @Autowired
    lateinit var gameStatsRepository: GameStatsRepository

    @Autowired
    lateinit var playerRepository: PlayerRepository

    @Autowired
    lateinit var tournamentRepository: TournamentRepository

    @Autowired
    lateinit var teamRepository: TeamRepository

    @Autowired lateinit var matchRepository: MatchRepository

    override fun insertGameStats(@RequestBody gameStats: List<MatchStatsModel>): Int {
        gameStatsRepository.insert(gameStats)

        return gameStats.size
    }

    /**
     * This method returns a fixed number of match stats for a player,
     *
     * */
    override fun getGameStatsForPlayer(playerId: String): List<MatchStatsModel> {

/*
        return gameStatsRepository.findBySummonerID(playerId,PageRequest.of(0,25))
                .switchIfEmpty { throw NonExistingException("No stats for player $playerId") }
                .collectList()
                .blockOptional(Duration.ofMillis(BLOCK_OPTIONAL_TIME_OUT))
                .orElseGet { Collections.emptyList() }
*/

        TODO()
    }

    override fun getGameStatsForPlayer(playerId: String, pageSize: Int, pageLength: Int): List<MatchStatsModel> {
        TODO()
/*
        return gameStatsRepository.findByTeamID(playerId, PageRequest.of(pageSize, pageLength, Sort.by(Sort.Order.asc("date"))))
                .switchIfEmpty { throw NonExistingException("An error occurred in getting the game stats for $playerId") }
                .collectList()
                .blockOptional(Duration.ofMillis(BLOCK_OPTIONAL_TIME_OUT))
                .orElseThrow { throw TimeOutException("Operation timed out") }
*/
    }

    override fun getGameStatsForTeam(teamID: String): List<MatchStatsModel> {
/*
        val team:Teams = teamRepository.findByTeamID(teamID).orElseThrow { throw NonExistingException("No such stats found for $teamID") }

        team.teamID.let {
            return gameStatsRepository.findByTeamID(teamID, PageRequest.of(1, 25, Sort.by(Sort.Order.asc("date")))).orElseThrow { throw NonExistingException("An error occurred in getting the game stats for ${team.teamName}") }
        }
*/
        TODO()
    }

    override fun getGameStatsForTeam(teamID: String, pageSize: Int, pageLength: Int): List<MatchStatsModel> {
/*
        val team:Teams = teamRepository.findByTeamID(teamID).orElseThrow { throw NonExistingException("No such stats found for $teamID") }
        team.teamID.let {
            return gameStatsRepository.findByTeamID(teamID, PageRequest.of(pageSize, pageLength, Sort.by(Sort.Order.asc("date"))))
                    .orElseThrow { throw NonExistingException("An error occurred in getting the game stats for ${team.teamName}") }
        }
*/
        TODO()
    }

    /**I am supposed to validate this information.
     *
     * No need to validate the information.
     * */
    override fun getGameStatsForPlayerName(playerName: String): List<MatchStatsModel> {
        return gameStatsRepository.findByPlayer(playerName, PageRequest.of(0,25, Sort.by(Sort.Order.asc("date"))))
                .collectList()
                .blockOptional(Duration.ofMillis(BLOCK_OPTIONAL_TIME_OUT))
                .orElseThrow { throw NonExistingException("An error occurred in getting the game stats for $playerName") }
    }

    override fun getGameStatsForPlayerName(playerName: String, pageSize: Int, pageLength: Int): List<MatchStatsModel> {

        return gameStatsRepository.findByPlayer(playerName, PageRequest.of(pageSize,pageLength, Sort.by(Sort.Order.asc("date"))))
                .switchIfEmpty { throw  NonExistingException("An error occurred in getting the game stats for $playerName")}
                .collectList()
                .blockOptional(Duration.ofMillis(BLOCK_OPTIONAL_TIME_OUT))
                .orElseThrow { throw TimeOutException("A time out error occurred in getting the game stats for $playerName") }

    }

    /**
     * Get the Game stats for a teamName.
     *
     * So first we check if the team is available
     *
     * We take the first team on the list, because we can have the same team, over and over, we duplicate the DB so we can
     * get more information about what tournament they play over time.
     * */
    override fun getGameStatsForTeamName(teamName: String): List<MatchStatsModel> {
        return gameStatsRepository.findByTeam(teamName, PageRequest.of(0,25))
                .switchIfEmpty { throw NonExistingException("No stats for $teamName")}
                .collectList()
                .blockOptional(Duration.ofMillis(BLOCK_OPTIONAL_TIME_OUT))
                .orElseThrow { throw TimeOutException("Operation timed out") }
    }

    override fun getGameStatsForTeamName(teamName: String, page: Int, size: Int): List<MatchStatsModel> {
            /*
        val team:Teams = teamRepository.findByTeamName(teamName).orElseThrow { throw NonExistingException("No team name found for $teamName") }
        team.teamName.let {
            return gameStatsRepository.findByTeamName(it, PageRequest.of(pageSize,pageLength))
                    .orElseThrow { throw NonExistingException("No stats for $teamName") }
        }
*/
        TODO()
    }

    /**
     * Here, we get the player stats for a tournament.
     *
     * We first have to check if the tournament is available and if the player is available
     * Then we check if the player and tournament can be found in the MatchStatsModel, before returning it
     * This must be paginated.
     *
     * This REST service must be able to withstand lots of requests at a time and its important that we do not over crowd the
     * service with code that hinders performance
     * */
    override fun getPlayerGameStatsForTournament(playerName: String, tournamentID: String, page: Int, size: Int): List<MatchStatsModel> {

        val o = gameStatsRepository.findByPlayer(playerName,PageRequest.of(page, size))
                .switchIfEmpty { throw NonExistingException("No such game stats found for $playerName") }
                .collectList()
                .blockOptional(Duration.ofMillis(BLOCK_OPTIONAL_TIME_OUT))
                .orElseThrow { throw TimeOutException("Operation timed out, for $playerName") }

        return o
    }

}