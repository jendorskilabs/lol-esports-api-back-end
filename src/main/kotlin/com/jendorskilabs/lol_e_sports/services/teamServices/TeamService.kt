package com.jendorskilabs.lol_e_sports.services.teamServices

import com.jendorskilabs.lol_e_sports.controllers.errorExceptions.NonExistingException
import com.jendorskilabs.lol_e_sports.model.teamsModel.Teams
import com.jendorskilabs.lol_e_sports.repository.teamRepository.TeamRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.web.bind.annotation.RequestBody
import javax.validation.Valid

/**
 * This should not return the BaseResponseModel
 * */
interface TeamService {

    fun insertAllTeams(teamList: List<Teams>):String

    fun getAllTeams(): List<Teams>

    fun getAllTeamsPaginated(page:Int, size:Int): List<Teams>

    fun getTeamsForALeague(leagueID:String):List<Teams>

    fun getTeamsForAMatch(tournamentID: String, matchID:String):List<Teams>

    fun getATeam(teamSlug:String):Teams

    fun getTeamByID(teamID:String):Teams

    fun getTeamsForAGame(gameID: String):List<Teams>

}

@Service("teamService")
class TeamServiceImpl :TeamService{

    @Autowired
    lateinit var teamRepository: TeamRepository

    /**
     * We have to be sure the List contains the original information we need
     * */
    override fun insertAllTeams(@RequestBody(required = true) @Valid teamList: List<Teams>): String {

        teamRepository.saveAll(teamList)

        return "Saved Successfully"
    }

    override fun getAllTeams(): List<Teams> {
        return teamRepository.findAll()
                .collectList()
                .blockOptional()
                .orElseThrow { throw NonExistingException("An error occurred in getting teams") }
    }

    override fun getAllTeamsPaginated(page: Int, size: Int): List<Teams> {
        TODO("Not yet implemented")
    }

    override fun getTeamsForALeague(leagueID: String): List<Teams> {
        TODO("Not yet implemented")
    }

    /**
     * Here, we get the teams that played a match in a tournament.
     * This has to mean, the teamID for all the teams on display, MUST be different.
     * This request, must be paginated.
     *
     * So we confirm if the tournamnet and the match is in our DB, then we */
    override fun getTeamsForAMatch(tournamentID: String, matchID: String): List<Teams> {
        TODO("Not yet implemented")
    }

    override fun getATeam(teamSlug: String): Teams {
        TODO("Not yet implemented")
    }

    override fun getTeamByID(teamID: String): Teams {
        TODO("Not yet implemented")
    }

    override fun getTeamsForAGame(gameID: String): List<Teams> {
        TODO("Not yet implemented")
    }

}