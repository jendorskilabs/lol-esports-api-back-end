package com.jendorskilabs.lol_e_sports

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class LoLESportsApplication

fun main(args: Array<String>) {
	runApplication<LoLESportsApplication>(*args)
}
